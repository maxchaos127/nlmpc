
#ifndef NLMPC_OPT_SUNC_H
#define NLMPC_OPT_SUNC_H

#include "nlmpc/nlmpc.h"
#include "nlmpc/opt.h"

typedef struct nlmpc_optsunc_problem_t nlmpc_optsunc_problem_t;
typedef struct nlmpc_optsunc_solution_t nlmpc_optsunc_solution_t;

typedef struct nlmpc_optsunc_optimizer_t nlmpc_optsunc_optimizer_t;
typedef nlmpc_opt_optimizer_resources_t nlmpc_optsunc_optimizer_resources_t;
typedef nlmpc_opt_optimizer_options_t nlmpc_optsunc_optimizer_options_t;
typedef nlmpc_opt_optimizer_status_t nlmpc_optsunc_optimizer_status_t;

typedef
void
(*nlmpc_optsunc_optimizer_solver_t) (nlmpc_optsunc_optimizer_t *optimizer,
				     nlmpc_optsunc_solution_t *solution,
				     nlmpc_optsunc_optimizer_status_t *status);

typedef
int
(*nlmpc_optsunc_optimizer_alloc_t) (nlmpc_optsunc_optimizer_t *optimizer,
				    nlmpc_optsunc_problem_t *problem);

typedef
void
(*nlmpc_optsunc_optimizer_free_t) (nlmpc_optsunc_optimizer_t *optimizer);



typedef
void
(*nlmpc_optsunc_costfunc_t) (nlmpc_optsunc_solution_t *solution);

typedef
void
(*nlmpc_optsunc_simulator_t) (nlmpc_optsunc_solution_t *solution,
			      const unsigned int step_init);

typedef
int
(*nlmpc_optsunc_solution_cstr_t) (const nlmpc_optsunc_problem_t *problem,
				  nlmpc_optsunc_solution_t *solution);

typedef
void
(*nlmpc_optsunc_solution_dstr_t) (nlmpc_optsunc_solution_t *solution);

/* typedef */
/* int */
/* (*nlmpc_optsunc_solution_init_t) (nlmpc_optsunc_solution_t *solution); */

typedef
void
(*nlmpc_optsunc_solution_copy_t) (const nlmpc_optsunc_solution_t *solution_source,
				  nlmpc_optsunc_solution_t *solution_target);

/* typedef */
/* int */
/* (*nlmpc_optsunc_solution_eval_t) (const nlmpc_optsunc_solution_t *solution); */

typedef
int
(*nlmpc_optsunc_timeout_t) (const nlmpc_optsunc_problem_t *problem);



struct nlmpc_optsunc_optimizer_t {
  nlmpc_optsunc_problem_t *problem;
  nlmpc_optsunc_optimizer_solver_t solver;
  nlmpc_optsunc_optimizer_options_t *options;
  nlmpc_optsunc_optimizer_resources_t *resources;
  nlmpc_optsunc_optimizer_alloc_t optimizer_alloc;
  nlmpc_optsunc_optimizer_free_t optimizer_free;
};

struct nlmpc_optsunc_problem_t
{
  unsigned int steps_amount;
  unsigned int steps_init;
  unsigned int steps_final;
  unsigned int states_dim;
  unsigned int inputs_dim;
  unsigned int states_amount;
  unsigned int inputs_amount;
  nlmpc_optsunc_costfunc_t costfunc;
  nlmpc_optsunc_simulator_t simulator;
  nlmpc_optsunc_solution_cstr_t solution_cstr;
  nlmpc_optsunc_solution_dstr_t solution_dstr;
  /* nlmpc_optsunc_solution_init_t solution_init; */
  nlmpc_optsunc_solution_copy_t solution_copy;
  /* nlmpc_optsunc_solution_eval_t solution_eval; */
  nlmpc_optsunc_timeout_t timeout;
  double *direction;
  void *parameters;
};

struct nlmpc_optsunc_solution_t
{
  nlmpc_optsunc_problem_t *problem;
  double *states;
  double *inputs;
  double cost;
  void *extras_var;
  void *extras_const;
};

#endif /* NLMPC_OPT_SUNC_H */
