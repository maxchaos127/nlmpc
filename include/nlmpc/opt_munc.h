
#ifndef NLMPC_OPT_MUNC_H
#define NLMPC_OPT_MUNC_H

#include "nlmpc/opt.h"
#include "nlmpc/opt_sunc.h"

typedef struct nlmpc_optmunc_problem_t nlmpc_optmunc_problem_t;
typedef struct nlmpc_optmunc_solution_t nlmpc_optmunc_solution_t;

typedef struct nlmpc_optmunc_optimizer_t nlmpc_optmunc_optimizer_t;
typedef nlmpc_opt_optimizer_resources_t nlmpc_optmunc_optimizer_resources_t;
typedef nlmpc_opt_optimizer_options_t nlmpc_optmunc_optimizer_options_t;
typedef nlmpc_opt_optimizer_status_t nlmpc_optmunc_optimizer_status_t;

typedef
void
(*nlmpc_optmunc_optimizer_solver_t) (nlmpc_optmunc_optimizer_t *optimizer,
				     nlmpc_optmunc_solution_t *solution,
				     nlmpc_optmunc_optimizer_status_t *status);

typedef
int
(*nlmpc_optmunc_optimizer_alloc_t) (nlmpc_optmunc_optimizer_t *optimizer,
				    nlmpc_optmunc_problem_t *problem);

typedef
void
(*nlmpc_optmunc_optimizer_free_t) (nlmpc_optmunc_optimizer_t *optimizer);



typedef
void
(*nlmpc_optmunc_costfunc_t) (nlmpc_optmunc_solution_t *solution);

typedef
void
(*nlmpc_optmunc_simulator_t) (nlmpc_optmunc_solution_t *solution,
			      const unsigned int step_init);

typedef
int
(*nlmpc_optmunc_solution_cstr_t) (const nlmpc_optmunc_problem_t *problem,
				  nlmpc_optmunc_solution_t *solution);

typedef
void
(*nlmpc_optmunc_solution_dstr_t) (nlmpc_optmunc_solution_t *solution);

/* typedef */
/* int */
/* (*nlmpc_optmunc_solution_init_t) (nlmpc_optmunc_solution_t *solution); */

typedef
void
(*nlmpc_optmunc_solution_copy_t) (const nlmpc_optmunc_solution_t *solution_source,
				  nlmpc_optmunc_solution_t *solution_target);

/* typedef */
/* int */
/* (*nlmpc_optmunc_solution_eval_t) (const nlmpc_optmunc_solution_t *solution); */

typedef
int
(*nlmpc_optmunc_timeout_t) (const nlmpc_optmunc_problem_t *problem);


struct nlmpc_optmunc_optimizer_t {
  nlmpc_optmunc_problem_t *problem;
  nlmpc_optmunc_optimizer_solver_t solver;
  nlmpc_optmunc_optimizer_options_t *options;
  nlmpc_optmunc_optimizer_resources_t *resources;
  nlmpc_optmunc_optimizer_alloc_t optimizer_alloc;
  nlmpc_optmunc_optimizer_free_t optimizer_free;
};

struct nlmpc_optmunc_problem_t
{
  unsigned int steps_amount;
  unsigned int steps_init;
  unsigned int steps_final;
  unsigned int states_dim;
  unsigned int inputs_dim;
  unsigned int states_amount;
  unsigned int inputs_amount;
  nlmpc_optmunc_costfunc_t costfunc;
  nlmpc_optmunc_simulator_t simulator;
  nlmpc_optmunc_solution_cstr_t solution_cstr;
  nlmpc_optmunc_solution_dstr_t solution_dstr;
  /* nlmpc_optmunc_solution_init_t solution_init; */
  nlmpc_optmunc_solution_copy_t solution_copy;
  /* nlmpc_optmunc_solution_eval_t solution_eval; */
  nlmpc_optmunc_timeout_t timeout;
  void *parameters;
};

struct nlmpc_optmunc_solution_t
{
  nlmpc_optmunc_problem_t *problem;
  double *states;
  double *inputs;
  double cost;
  void *extras_var;
  void *extras_const;
};

void
nlmpc_optmunc_costfunc_grad (const nlmpc_optmunc_solution_t *solution,
			     const double inputs_delta,
			     double *cost_grad,
			     nlmpc_optmunc_solution_t *solution_perturbed);

#endif /* NLMPC_OPT_MUNC_H */
