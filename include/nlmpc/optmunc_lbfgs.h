
#ifndef NLMPC_OPTMUNC_LBFGS_H
#define NLMPC_OPTMUNC_LBFGS_H

#include "nlmpc/nlmpc.h"
#include "nlmpc/opt_munc.h"
#include "nlmpc/opt_sunc.h"

typedef struct nlmpc_optmunc_lbfgs_options_t nlmpc_optmunc_lbfgs_options_t;
typedef struct nlmpc_optmunc_lbfgs_resources_t nlmpc_optmunc_lbfgs_resources_t;

struct nlmpc_optmunc_lbfgs_options_t {
  double gamma_init;
  double inputs_delta;
  unsigned int history_depth;
  unsigned int iter_amount_max;
  double tol_inputs_abs;
  double tol_cost_abs;
  /* double tol_cost_grad_abs; */
  nlmpc_optsunc_optimizer_solver_t sub_optimizer_solver;
  nlmpc_optsunc_optimizer_options_t *sub_optimizer_options;
  nlmpc_optsunc_optimizer_alloc_t sub_optimizer_alloc;
  nlmpc_optsunc_optimizer_free_t sub_optimizer_free;
};

struct nlmpc_optmunc_lbfgs_resources_t {
  nlmpc_optmunc_solution_t solution_next;
  double *inputs_prev;
  double *cost_grad_prev;
  double *cost_grad_next;
  unsigned int *indices;
  double *Rho; double *S; double *T; double *alpha; double *q;
  nlmpc_optmunc_solution_t solution_utls;
  nlmpc_optsunc_problem_t sub_problem;
  void *sub_solution_extras_var;
  void *sub_solution_extras_const;
  nlmpc_optsunc_optimizer_t sub_optimizer;
  nlmpc_optsunc_optimizer_status_t sub_optimizer_status;
};

void
nlmpc_optmunc_lbfgs_solver (nlmpc_optmunc_optimizer_t *optimizer,
			    nlmpc_optmunc_solution_t *solution,
			    nlmpc_optmunc_optimizer_status_t *status);

int
nlmpc_optmunc_lbfgs_alloc (nlmpc_optmunc_optimizer_t *optimizer,
			   nlmpc_optmunc_problem_t *problem);

void
nlmpc_optmunc_lbfgs_free (nlmpc_optmunc_optimizer_t *optimizer);

#endif /* NLMPC_OPTMUNC_LBFGS_H */
