
#ifndef NLMPC_OPTSUNC_INTERPQUAD_H
#define NLMPC_OPTSUNC_INTERPQUAD_H

#include "nlmpc/opt_sunc.h"

typedef
struct nlmpc_optsunc_interpquad_options_t
nlmpc_optsunc_interpquad_options_t;

typedef
struct nlmpc_optsunc_interpquad_resources_t
nlmpc_optsunc_interpquad_resources_t;

struct nlmpc_optsunc_interpquad_options_t {
  double x_init;
  double s_init;
  double x_min;
  double x_max;
  double s_factor_inc;
  double s_factor_dec;
  double relaxation;
  unsigned int iter_amount_max;
  double tol_dx_abs;
  double tol_dy_abs;
  double tol_approx_abs;
  double *inputs_vector_lb;
  double *inputs_vector_ub;
};

struct nlmpc_optsunc_interpquad_resources_t {
  nlmpc_optsunc_solution_t solution_next;
};

void
nlmpc_optsunc_interpquad_solver (nlmpc_optsunc_optimizer_t *optimizer,
				 nlmpc_optsunc_solution_t *solution,
				 nlmpc_optsunc_optimizer_status_t *status);

int
nlmpc_optsunc_interpquad_alloc (nlmpc_optsunc_optimizer_t *optimizer,
				nlmpc_optsunc_problem_t *problem);

void
nlmpc_optsunc_interpquad_free (nlmpc_optsunc_optimizer_t *optimizer);

#endif /* NLMPC_OPTSUNC_INTERPQUAD_H */
