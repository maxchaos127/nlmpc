
#ifndef NLMPC_MAIN_FIXEDTIME_H
#define NLMPC_MAIN_FIXEDTIME_H

#include "nlmpc/nlmpc.h"
#include "nlmpc/opt.h"
#include "nlmpc/opt_mc.h"

typedef
struct nlmpc_main_fixedtime_status_t nlmpc_main_fixedtime_status_t;
typedef
struct nlmpc_main_fixedtime_resources_t nlmpc_main_fixedtime_resources_t;
typedef
struct nlmpc_main_fixedtime_options_t nlmpc_main_fixedtime_options_t;

int
nlmpc_main_fixedtime (const nlmpc_problem_t *problem,
		      nlmpc_solution_t *solution,
		      nlmpc_main_fixedtime_status_t *status,
		      nlmpc_main_fixedtime_resources_t *resources,
		      const nlmpc_main_fixedtime_options_t *options);

struct nlmpc_main_fixedtime_status_t {
  int flag_exit_status;
  int flag_exit_code;
};

struct nlmpc_main_fixedtime_resources_t {
  nlmpc_optmc_optimizer_t sub_optimizer;
  nlmpc_optmc_optimizer_status_t sub_optimizer_status;
  nlmpc_optmc_problem_t sub_problem;
};

struct nlmpc_main_fixedtime_options_t {
  nlmpc_optmc_optimizer_solver_t sub_optimizer_solver;
  nlmpc_optmc_optimizer_options_t *sub_optimizer_options;
  nlmpc_optmc_optimizer_alloc_t sub_optimizer_alloc;
  nlmpc_optmc_optimizer_free_t sub_optimizer_free;
};

#endif /*** NLMPC_MAIN_FIXEDTIME_H ***/
