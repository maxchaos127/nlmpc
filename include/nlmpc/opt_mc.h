
#ifndef NLMPC_OPT_MC_H
#define NLMPC_OPT_MC_H

#include "nlmpc/opt.h"
#include "nlmpc/opt_munc.h"

typedef struct nlmpc_optmc_problem_t nlmpc_optmc_problem_t;
typedef struct nlmpc_optmc_solution_t nlmpc_optmc_solution_t;

typedef struct nlmpc_optmc_optimizer_t nlmpc_optmc_optimizer_t;
typedef nlmpc_opt_optimizer_resources_t nlmpc_optmc_optimizer_resources_t;
typedef nlmpc_opt_optimizer_options_t nlmpc_optmc_optimizer_options_t;
typedef nlmpc_opt_optimizer_status_t nlmpc_optmc_optimizer_status_t;



typedef
void
(*nlmpc_optmc_optimizer_solver_t) (nlmpc_optmc_optimizer_t *optimizer,
				   nlmpc_optmc_solution_t *solution,
				   nlmpc_optmc_optimizer_status_t *status);

typedef
int
(*nlmpc_optmc_optimizer_alloc_t) (nlmpc_optmc_optimizer_t *optimizer,
				  nlmpc_optmc_problem_t *problem);

typedef
void
(*nlmpc_optmc_optimizer_free_t) (nlmpc_optmc_optimizer_t *optimizer);



typedef
void
(*nlmpc_optmc_costfunc_t) (nlmpc_optmc_solution_t *solution);

typedef
void
(*nlmpc_optmc_gfunc_t) (nlmpc_optmc_solution_t *solution);

typedef
void
(*nlmpc_optmc_hfunc_t) (nlmpc_optmc_solution_t *solution);

typedef
void
(*nlmpc_optmc_simulator_t) (nlmpc_optmc_solution_t *solution,
			    const unsigned int step_init);

typedef
int
(*nlmpc_optmc_solution_cstr_t) (const nlmpc_optmc_problem_t *problem,
				nlmpc_optmc_solution_t *solution);

typedef
void
(*nlmpc_optmc_solution_dstr_t) (nlmpc_optmc_solution_t *solution);

/* typedef */
/* int */
/* (*nlmpc_optmc_solution_init_t) (nlmpc_optmc_solution_t *solution); */

typedef
void
(*nlmpc_optmc_solution_copy_t) (const nlmpc_optmc_solution_t *solution_source,
				nlmpc_optmc_solution_t *solution_target);

/* typedef */
/* int */
/* (*nlmpc_optmc_solution_eval_t) (const nlmpc_optmc_solution_t *solution); */

typedef
int
(*nlmpc_optmc_timeout_t) (const nlmpc_optmc_problem_t *problem);



struct nlmpc_optmc_optimizer_t {
  nlmpc_optmc_problem_t *problem;
  nlmpc_optmc_optimizer_solver_t solver;
  nlmpc_optmc_optimizer_status_t status;
  nlmpc_optmc_optimizer_options_t *options;
  nlmpc_optmc_optimizer_resources_t *resources;
  nlmpc_optmc_optimizer_alloc_t optimizer_alloc;
  nlmpc_optmc_optimizer_free_t optimizer_free;
};

struct nlmpc_optmc_problem_t
{
  unsigned int steps_amount;
  unsigned int steps_init;
  unsigned int steps_final;
  unsigned int states_dim;
  unsigned int inputs_dim;
  unsigned int states_amount;
  unsigned int inputs_amount;
  unsigned int g_amount;
  unsigned int h_amount;
  nlmpc_optmc_costfunc_t costfunc;
  nlmpc_optmc_gfunc_t gfunc;
  nlmpc_optmc_hfunc_t hfunc;
  nlmpc_optmc_simulator_t simulator;
  nlmpc_optmc_solution_cstr_t solution_cstr;
  nlmpc_optmc_solution_dstr_t solution_dstr;
  /* nlmpc_optmc_solution_init_t solution_init; */
  nlmpc_optmc_solution_copy_t solution_copy;
  /* nlmpc_optmc_solution_eval_t solution_eval; */
  nlmpc_optmc_timeout_t timeout;
  void *parameters;
};

struct nlmpc_optmc_solution_t
{
  nlmpc_optmc_problem_t *problem;
  double *states;
  double *inputs;
  double cost;
  double *g;
  double *h;
  void *extras_var;
  void *extras_const;
};

#endif /* NLMPC_OPT_MC_H */
