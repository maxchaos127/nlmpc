
#ifndef NLMPC_OPT_H
#define NLMPC_OPT_H

typedef void nlmpc_opt_optimizer_options_t;
typedef void nlmpc_opt_optimizer_resources_t;
typedef struct nlmpc_opt_optimizer_status_t nlmpc_opt_optimizer_status_t;

/*** flag_exit_status:
 ***    negative: invalid state (should not have happened).
 ***    0: failure,
 ***    1: success (better solution found),
 ***    2: success (best solution found)
 ***/
struct nlmpc_opt_optimizer_status_t {
  int flag_exit_status; 
  int flag_exit_code;
};

#endif /* NLMPC_OPT_H */
