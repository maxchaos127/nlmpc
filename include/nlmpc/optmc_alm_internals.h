
#ifndef NLMPC_OPTMC_ALM_INTERNALS_H
#define NLMPC_OPTMC_ALM_INTERNALS_H

#include "nlmpc/optmc_alm.h"

typedef struct alm_sub_parameters_t alm_sub_parameters_t;
struct alm_sub_parameters_t {
  nlmpc_optmc_alm_multipliers_t *multipliers;
  /* double *lambda_g; */
  /* double *lambda_h; */
  /* double r; */
  nlmpc_optmc_problem_t *super_problem;
};

typedef struct alm_sub_extras_var_t alm_sub_extras_var_t;
struct alm_sub_extras_var_t {
  double cost;
  double *g;
  double *h;
  void *super_extras_var;
};

typedef struct alm_sub_extras_const_t alm_sub_extras_const_t;
struct alm_sub_extras_const_t {
  void *super_extras_const;
};

void
alm_sub_costfunc (nlmpc_optmunc_solution_t *solution);

void
alm_sub_simulator (nlmpc_optmunc_solution_t *solution,
		   const unsigned int steps_init);

int
alm_sub_solution_cstr (const nlmpc_optmunc_problem_t *problem,
		       nlmpc_optmunc_solution_t *solution);

void
alm_sub_solution_dstr (nlmpc_optmunc_solution_t *solution);

/* int */
/* alm_sub_solution_init (nlmpc_optmunc_solution_t *solution); */

void
alm_sub_solution_copy (const nlmpc_optmunc_solution_t *solution_source,
		       nlmpc_optmunc_solution_t *solution_target);

/* int */
/* alm_sub_solution_eval (const nlmpc_optmunc_solution_t *solution); */

int
alm_sub_timeout (const nlmpc_optmunc_problem_t *problem);

#endif /* NLMPC_OPTMC_ALM_INTERNALS_H */
