
#ifndef NLMPC_H
#define NLMPC_H

typedef struct nlmpc_problem_t nlmpc_problem_t;
typedef struct nlmpc_solution_t nlmpc_solution_t;

/**** The purpose of this function is to calculate
 **** the cost
 **** of the given solution.
 **** This function must calculate and modify ONLY
 ****  * solution -> cost
 **** This function is allowed to depend on:
 ****  * solution -> problem -> states_dim
 ****  * solution -> problem -> inputs_dim
 ****  * solution -> steps_amount
 ****  * solution -> steps_init
 ****  * solution -> steps_final
 ****  * solution -> states_amount
 ****  * solution -> inputs_amount
 ****  * solution -> g_amount
 ****  * solution -> h_amount
 ****  * solution -> states
 ****  * solution -> inputs
 ****  * solution -> extras_var
 ****  * solution -> extras_const
 **** This function is forbidden to depend on
 ****  * solution -> cost
 ****  * solution -> g
 ****  * solution -> h
 ****/
typedef
void
(*nlmpc_costfunc_t) (nlmpc_solution_t *solution);

/**** The purpose of this function is to calculate
 **** the equality constraints
 **** of the given solution.
 **** This function must calculate and modify ONLY
 ****  * solution -> g
 **** This function is allowed to depend on:
 ****  * solution -> problem -> states_dim
 ****  * solution -> problem -> inputs_dim
 ****  * solution -> steps_amount
 ****  * solution -> steps_init
 ****  * solution -> steps_final
 ****  * solution -> states_amount
 ****  * solution -> inputs_amount
 ****  * solution -> g_amount
 ****  * solution -> h_amount
 ****  * solution -> states
 ****  * solution -> inputs
 ****  * solution -> extras_var
 ****  * solution -> extras_const
 **** This function is forbidden to depend on
 ****  * solution -> cost
 ****  * solution -> g
 ****  * solution -> h
 ****/
typedef
void
(*nlmpc_gfunc_t) (nlmpc_solution_t *solution);

/**** The purpose of this function is to calculate
 **** the inequality constraints
 **** of the given solution.
 **** This function must calculate and modify ONLY
 ****  * solution -> h
 **** This function is allowed to depend on:
 ****  * solution -> problem -> states_dim
 ****  * solution -> problem -> inputs_dim
 ****  * solution -> steps_amount
 ****  * solution -> steps_init
 ****  * solution -> steps_final
 ****  * solution -> states_amount
 ****  * solution -> inputs_amount
 ****  * solution -> g_amount
 ****  * solution -> h_amount
 ****  * solution -> states
 ****  * solution -> inputs
 ****  * solution -> extras_var
 ****  * solution -> extras_const
 **** This function is forbidden to depend on
 ****  * solution -> cost
 ****  * solution -> g
 ****  * solution -> h
 ****/
typedef
void
(*nlmpc_hfunc_t) (nlmpc_solution_t *solution);

/**** The purpose of this function is to simulate
 **** the behavior of the system.
 **** This function must calculate and modify ONLY
 ****  * solution -> states,
 ****    corresponding to
 ****    {steps_init + 1, steps_init + 2, ..., steps_amount},
 ****  * solution -> extras_var,
 ****    depending on
 ****    {steps_init, steps_init + 2, ..., steps_amount},
 **** The following must hold:
 ****  * steps_init >= 0
 ****  * steps_init <= (solution -> steps_amount) - 1
 **** This function is allowed to depend on:
 ****  * problem
 ****  * solution -> steps_amount
 ****  * solution -> steps_init
 ****  * solution -> steps_final
 ****  * solution -> states_amount
 ****  * solution -> inputs_amount
 ****  * solution -> g_amount
 ****  * solution -> h_amount
 ****  * solution -> states,
 ****    for only {0, 1, ..., steps_init}
 ****  * solution -> inputs
 ****  * solution -> extras_var,
 ****    depending on only {0, 1, ..., steps_init - 1},
 ****  * solution -> extras_const
 **** This function is forbidden to depend on
 ****  * solution -> states
 ****    corresponding to
 ****    {steps_init + 1, steps_init + 2, ...,
 ****     solution -> steps_amount},
 ****  * solution -> cost
 ****  * solution -> g
 ****  * solution -> h
 ****  * solution -> extras_var
 ****    depending on
 ****    {steps_init, steps_init + 1, ...,
 ****     solution -> steps_amount}.
 ****/
typedef
void
(*nlmpc_simulator_t) (nlmpc_solution_t *solution,
		      const unsigned int steps_init);

/**** The purpose of this function is to construct a solution.
 **** This function must ONLY
 ****  * set
 ****    * solution -> problem
 ****    * solution -> steps_amount
 ****    * solution -> steps_init
 ****    * solution -> steps_final
 ****  * calculate
 ****    * solution -> states_amount
 ****    * solution -> inputs_amount
 ****    * solution -> g_amount
 ****    * solution -> h_amount
 ****  * allocate and set
 ****    * solution -> states
 ****    * solution -> inputs
 ****    * solution -> g
 ****    * solution -> h
 ****    * solution -> extras_var
 ****    * solution -> extras_const
 ****    to a default state.
 **** This function is allowed to depend on:
 ****  * problem
 ****  * steps_amount
 **** This function is forbidden to depend on
 ****  * solution
 ****/
typedef
int
(*nlmpc_solution_cstr_t) (const nlmpc_problem_t *problem,
			  nlmpc_solution_t *solution,
			  const unsigned int steps_amount,
			  const unsigned int steps_init,
			  const unsigned int steps_final);

/**** The purpose of this function is to destruct a solution.
 **** This function must ONLY
 ****  * unset
 ****    * solution -> problem
 ****    * solution -> steps_amount
 ****    * solution -> steps_init
 ****    * solution -> steps_final
 ****    * solution -> states_amount
 ****    * solution -> inputs_amount
 ****    * solution -> g_amount
 ****    * solution -> h_amount
 ****  * deallocate
 ****    * solution -> states
 ****    * solution -> inputs
 ****    * solution -> g
 ****    * solution -> h
 ****    * solution -> extras_var
 ****    * solution -> extras_const
 **** This function is allowed to depend on:
 ****  * solution
 ****/
typedef
void
(*nlmpc_solution_dstr_t) (nlmpc_solution_t *solution);

/**** The purpose of this function is to initialize a solution.
 **** Initial values for states, inputs, etc,
 **** should be passed to this function through
 **** problem -> parameters.
 **** This function must ONLY calculate and set
 ****  * solution -> states
 ****  * solution -> inputs
 ****  * solution -> cost
 ****  * solution -> g
 ****  * solution -> h
 ****  * solution -> extras_var
 ****  * solution -> extras_const
 **** It is allowed to depend on:
 ****  * solution -> problem
 ****  * solution -> steps_amount
 ****  * solution -> steps_init
 ****  * solution -> steps_final
 ****  * solution -> states_amount
 ****  * solution -> inputs_amount
 ****  * solution -> g_amount
 ****  * solution -> h_amount
 **** This function is forbidden to depend on
 ****  * solution -> states
 ****  * solution -> inputs
 ****  * solution -> cost
 ****  * solution -> g
 ****  * solution -> h
 ****  * solution -> extras_var
 ****  * solution -> extras_const
 ****/
typedef
void
(*nlmpc_solution_init_t) (nlmpc_solution_t *solution);

/**** The purpose of this function is
 **** to copy the values of solution_source to solution_target.
 **** Both solution_source and solution_target
 **** must be allocated solutions
 **** for the same problem and horizon.
 **** Also, it must be a deep copy of the solutions.
 **** This function must ONLY copy
 ****  * solution_source -> states
 ****    to
 ****    solution_target -> states
 ****  * solution_source -> inputs
 ****    to
 ****    solution_target -> inputs
 ****  * solution_source -> cost
 ****    to
 ****    solution_target -> cost
 ****  * solution_source -> g
 ****    to
 ****    solution_target -> g
 ****  * solution_source -> h
 ****    to
 ****    solution_target -> h
 ****  * solution_source -> extras_var
 ****    to
 ****    solution_target -> extras_var
 ****  * solution_source -> extras_const
 ****    to
 ****    solution_target -> extras_const
 **** This function is allowed to depend on:
 ****  * solution_source
 ****  * solution_target -> problem
 ****  * solution_target -> steps_amount
 ****  * solution_target -> steps_init
 ****  * solution_target -> steps_final
 ****/
typedef
void
(*nlmpc_solution_copy_t) (const nlmpc_solution_t *solution_source,
			  nlmpc_solution_t *solution_target);

/* typedef */
/* int */
/* (*nlmpc_solution_eval_t) (const nlmpc_solution_t *solution); */

typedef
int
(*nlmpc_timeout_t) (const nlmpc_problem_t *problem);

struct nlmpc_problem_t
{
  unsigned int states_dim;
  unsigned int inputs_dim;
  nlmpc_costfunc_t costfunc;
  nlmpc_gfunc_t gfunc;
  nlmpc_hfunc_t hfunc;
  nlmpc_simulator_t simulator;
  nlmpc_solution_cstr_t solution_cstr;
  nlmpc_solution_dstr_t solution_dstr;
  nlmpc_solution_init_t solution_init;
  nlmpc_solution_copy_t solution_copy;
  /* nlmpc_solution_eval_t solution_eval; */
  nlmpc_timeout_t timeout;
  void *parameters;
};

struct nlmpc_solution_t
{
  nlmpc_problem_t *problem;
  unsigned int steps_amount;
  unsigned int steps_init;
  unsigned int steps_final;
  unsigned int states_amount;
  unsigned int inputs_amount;
  unsigned int g_amount;
  unsigned int h_amount;
  double *states;
  double *inputs;
  double cost;
  double *g;
  double *h;
  void *extras_var;
  void *extras_const;
};

#endif /* NLMPC_H */
