
#ifndef NLMPC_MAIN_INTERNALS_H
#define NLMPC_MAIN_INTERNALS_H

#include "nlmpc/opt_mc.h"

typedef struct submc_parameters_t submc_parameters_t;
typedef struct submc_extras_var_t submc_extras_var_t;
typedef struct submc_extras_const_t submc_extras_const_t;

void
submc_costfunc (nlmpc_optmc_solution_t *solution);

void
submc_gfunc (nlmpc_optmc_solution_t *solution);

void
submc_hfunc (nlmpc_optmc_solution_t *solution);

void
submc_simulator (nlmpc_optmc_solution_t *solution,
		 const unsigned int steps_init);

int
submc_solution_cstr (const nlmpc_optmc_problem_t *problem,
		     nlmpc_optmc_solution_t *solution);

void
submc_solution_dstr (nlmpc_optmc_solution_t *solution);

void
submc_solution_copy (const nlmpc_optmc_solution_t *solution_source,
		     nlmpc_optmc_solution_t *solution_target);

int
submc_timeout (const nlmpc_optmc_problem_t *problem);

/* int */
/* submc_solution_eval (const nlmpc_optmc_solution_t *solution); */

/* void */
/* submc_solution_init (nlmpc_optmc_solution_t *solution); */

struct submc_parameters_t {
  nlmpc_problem_t *super_problem;
};

struct submc_extras_var_t {
  void *super_extras_var;
};

struct submc_extras_const_t {
  void *super_extras_const;
};

#endif /* NLMPC_MAIN_INTERNALS_H */
