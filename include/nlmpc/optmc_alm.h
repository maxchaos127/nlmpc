
#ifndef NLMPC_OPTMC_ALM_H
#define NLMPC_OPTMC_ALM_H

#include "nlmpc/opt_mc.h"
#include "nlmpc/opt_munc.h"

typedef struct nlmpc_optmc_alm_options_t nlmpc_optmc_alm_options_t;
typedef struct nlmpc_optmc_alm_resources_t nlmpc_optmc_alm_resources_t;

typedef struct nlmpc_optmc_alm_parameters_t nlmpc_optmc_alm_parameters_t;

typedef struct nlmpc_optmc_alm_multipliers_t nlmpc_optmc_alm_multipliers_t; 
struct nlmpc_optmc_alm_multipliers_t {
  double r;
  double *lambda_g;
  double *lambda_h;
};

struct nlmpc_optmc_alm_options_t {
  double r_value_min;
  double r_value_max;
  double r_factor;
  unsigned int iter_amount_max;
  double tol_states_abs;
  double tol_inputs_abs;
  double tol_g_abs;
  double tol_h_abs;
  nlmpc_optmunc_optimizer_solver_t sub_optimizer_solver;
  nlmpc_optmunc_optimizer_options_t *sub_optimizer_options;
  nlmpc_optmunc_optimizer_alloc_t sub_optimizer_alloc;
  nlmpc_optmunc_optimizer_free_t sub_optimizer_free;
};

struct nlmpc_optmc_alm_resources_t {
  nlmpc_optmc_solution_t solution_next;
  double *states_prev;
  double *inputs_prev;
  double *g_prev;
  double *h_prev;
  nlmpc_optmc_alm_multipliers_t multipliers;
  nlmpc_optmunc_problem_t sub_problem;
  void *sub_solution_extras_var;
  void *sub_solution_extras_const;
  nlmpc_optmunc_optimizer_t sub_optimizer;
  nlmpc_optmunc_optimizer_status_t sub_optimizer_status;
};

void
nlmpc_optmc_alm_solver (nlmpc_optmc_optimizer_t *optimizer,
			nlmpc_optmc_solution_t *solution,
			nlmpc_optmc_optimizer_status_t *status);

int
nlmpc_optmc_alm_alloc (nlmpc_optmc_optimizer_t *optimizer,
		       nlmpc_optmc_problem_t *problem);

void
nlmpc_optmc_alm_free (nlmpc_optmc_optimizer_t *optimizer);

#endif /* NLMPC_OPTMC_ALM_H */
