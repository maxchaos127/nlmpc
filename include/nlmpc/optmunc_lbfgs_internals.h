
#ifndef NLMPC_OPTMUNC_LBFGS_INTERNALS_H
#define NLMPC_OPTMUNC_LBFGS_INTERNALS_H

#include "nlmpc/optmunc_lbfgs.h"

typedef struct lbfgs_sub_parameters_t lbfgs_sub_parameters_t;
struct lbfgs_sub_parameters_t {
  nlmpc_optmunc_problem_t *super_problem;
};

typedef struct lbfgs_sub_extras_var_t lbfgs_sub_extras_var_t;
struct lbfgs_sub_extras_var_t {
  void *super_extras_var;
};

typedef struct lbfgs_sub_extras_const_t lbfgs_sub_extras_const_t;
struct lbfgs_sub_extras_const_t {
  void *super_extras_const;
};

void
lbfgs_sub_costfunc (nlmpc_optsunc_solution_t *solution);

void
lbfgs_sub_simulator (nlmpc_optsunc_solution_t *solution,
		     const unsigned int steps_init);

int
lbfgs_sub_solution_cstr (const nlmpc_optsunc_problem_t *problem,
			 nlmpc_optsunc_solution_t *solution);

void
lbfgs_sub_solution_dstr (nlmpc_optsunc_solution_t *solution);

/* int */
/* lbfgs_sub_solution_init (nlmpc_optsunc_solution_t *solution); */

void
lbfgs_sub_solution_copy (const nlmpc_optsunc_solution_t *solution_source,
			 nlmpc_optsunc_solution_t *solution_target);

/* int */
/* lbfgs_sub_solution_eval (const nlmpc_optsunc_solution_t *solution); */

int
lbfgs_sub_timeout (const nlmpc_optsunc_problem_t *problem);

#endif /* NLMPC_OPTMUNC_LBFGS_INTERNALS_H */
