
#include "nlmpc/optmc_alm_internals.h"
#include <stdlib.h>

int
nlmpc_optmc_alm_alloc (nlmpc_optmc_optimizer_t *_optimizer,
		       nlmpc_optmc_problem_t *_problem)
{
  int fs;
  int flag_alloc;
  nlmpc_optmc_optimizer_t *optimizer = _optimizer;
  nlmpc_optmc_problem_t *problem = _problem;
  nlmpc_optmc_alm_options_t *options = optimizer -> options;
  nlmpc_optmc_alm_resources_t *resources;
  alm_sub_parameters_t *sub_problem_parameters;
  alm_sub_extras_var_t *sub_solution_extras_var;
  alm_sub_extras_const_t *sub_solution_extras_const;
  /*** Set flag of allocations to success. ***/
  flag_alloc = 0;
  /*** Allocate resources. ***/
  resources = NULL;
  if (! flag_alloc)
    {
      resources =
	(nlmpc_optmc_alm_resources_t*)
	malloc (sizeof (nlmpc_optmc_alm_resources_t));
      if (resources)
      	{
	  resources -> solution_next.problem = NULL;
      	  resources -> states_prev = NULL;
      	  resources -> inputs_prev = NULL;
      	  resources -> g_prev = NULL;
      	  resources -> h_prev = NULL;
      	  resources -> multipliers.r = 0.0;
	  resources -> multipliers.lambda_g = NULL;
	  resources -> multipliers.lambda_h = NULL;
	  resources -> sub_optimizer.problem = NULL;
      	  resources -> sub_problem.parameters = NULL;
	  resources -> sub_solution_extras_var = NULL;
	  resources -> sub_solution_extras_const = NULL;
      	}
      else
      	flag_alloc = 1;
    }
  if (! flag_alloc)
    {
      fs = problem -> solution_cstr
	(problem, & (resources -> solution_next));
      if (fs)
	flag_alloc = 2;
    }
  if ((! flag_alloc) && (problem -> states_amount))
    {
      resources -> states_prev =
	(double*) malloc ((problem -> states_amount) * sizeof (double));
      if (! (resources -> states_prev))
	flag_alloc = 3;
    }
  if ((! flag_alloc) && (problem -> inputs_amount))
    {
      resources -> inputs_prev =
	(double*) malloc ((problem -> inputs_amount) * sizeof (double));
      if (! (resources -> inputs_prev))
	flag_alloc = 4;
    }
  if ((! flag_alloc) && (problem -> g_amount))
    {
      resources -> g_prev =
	(double*) malloc ((problem -> g_amount) *sizeof (double));
      if (! (resources -> g_prev))
	flag_alloc = 5;
    }
  if ((! flag_alloc) && (problem -> h_amount))
    {
      resources -> h_prev =
	(double*) malloc ((problem -> h_amount) * sizeof (double));
      if (! (resources -> h_prev))
	flag_alloc = 6;
    }
  if ((! flag_alloc) && (problem -> g_amount))
    {
      resources -> multipliers.lambda_g =
	(double*) malloc ((problem -> g_amount) * sizeof (double));
      if (! (resources -> multipliers.lambda_g))
	flag_alloc = 7;
    }
  if ((! flag_alloc) && (problem -> h_amount))
    {
      resources -> multipliers.lambda_h =
	(double*) malloc ((problem -> h_amount) * sizeof (double));
      if (! (resources -> multipliers.lambda_h))
	flag_alloc = 8;
    }
  if (! flag_alloc)
    {
      sub_problem_parameters =
	(alm_sub_parameters_t*) malloc (sizeof (alm_sub_parameters_t));
      resources -> sub_problem.parameters = sub_problem_parameters;
      if (! sub_problem_parameters)
	flag_alloc = 9;
    }
  if (! flag_alloc)
    {
      sub_solution_extras_var =
	(alm_sub_extras_var_t*) malloc (sizeof (alm_sub_extras_var_t));
      resources -> sub_solution_extras_var = sub_solution_extras_var;
      if (! sub_solution_extras_var)
	flag_alloc = 10;
    }
  if (! flag_alloc)
    {
      sub_solution_extras_const =
	(alm_sub_extras_const_t*) malloc (sizeof (alm_sub_extras_const_t));
      resources -> sub_solution_extras_const = sub_solution_extras_const;
      if (! sub_solution_extras_const)
	flag_alloc = 11;
    }
  if (! flag_alloc)
    {
      resources -> sub_problem.steps_amount = problem -> steps_amount;
      resources -> sub_problem.steps_init = problem -> steps_init;
      resources -> sub_problem.steps_final = problem -> steps_final;
      resources -> sub_problem.states_dim = problem -> states_dim;
      resources -> sub_problem.inputs_dim = problem -> inputs_dim;
      resources -> sub_problem.states_amount = problem -> states_amount;
      resources -> sub_problem.inputs_amount = problem -> inputs_amount;
      resources -> sub_problem.costfunc = alm_sub_costfunc;
      resources -> sub_problem.simulator = alm_sub_simulator;
      resources -> sub_problem.solution_cstr = alm_sub_solution_cstr;
      resources -> sub_problem.solution_dstr = alm_sub_solution_dstr;
      resources -> sub_problem.solution_copy = alm_sub_solution_copy;
      /* resources -> sub_problem.solution_eval = alm_sub_solution_eval; */
      resources -> sub_problem.timeout = alm_sub_timeout;
      sub_problem_parameters -> multipliers = & resources -> multipliers;
      sub_problem_parameters -> super_problem = problem;
    }
  if (! flag_alloc)
    {
      resources -> sub_optimizer.problem = NULL;
      resources -> sub_optimizer.solver = options -> sub_optimizer_solver;
      resources -> sub_optimizer.options = options -> sub_optimizer_options;
      resources -> sub_optimizer.resources = NULL;
      resources -> sub_optimizer.optimizer_alloc =
	options -> sub_optimizer_alloc;
      resources -> sub_optimizer.optimizer_free =
	options -> sub_optimizer_free;
      fs = resources -> sub_optimizer.optimizer_alloc
	(& (resources -> sub_optimizer), & (resources -> sub_problem));
      if (fs)
	flag_alloc = 12;
    }
  if (! flag_alloc)
    {
      resources -> sub_optimizer_status.flag_exit_status = -1;
      resources -> sub_optimizer_status.flag_exit_code = -1;
    }
  /*** Check whether or not the allocations where successful.
   *** If false, then deallocate everything and return failure.
   ***/
  if (flag_alloc)
    {
      if (resources)
	{
	  if (resources -> sub_optimizer.problem)
	    {
	      resources -> sub_optimizer.optimizer_free
		(& (resources -> sub_optimizer));
	    }
	  if (resources -> sub_solution_extras_const)
	    free (resources -> sub_solution_extras_const);
	  if (resources -> sub_solution_extras_var)
	    free (resources -> sub_solution_extras_var);
	  if (resources -> sub_problem.parameters)
	    free (resources -> sub_problem.parameters);
	  if (resources -> multipliers.lambda_g)
	    free (resources -> multipliers.lambda_g);
	  if (resources -> multipliers.lambda_h)
	    free (resources -> multipliers.lambda_h);
	  if (resources -> h_prev)
	    free (resources -> h_prev);
	  if (resources -> g_prev)
	    free (resources -> g_prev);
	  if (resources -> inputs_prev)
	    free (resources -> inputs_prev);
	  if (resources -> states_prev)
	    free (resources -> states_prev);
	  problem -> solution_dstr (& (resources -> solution_next));
	  free (resources);
	}
      return flag_alloc;
    }
  optimizer -> problem = problem;
  optimizer -> resources = resources;
  return 0;
}

void
nlmpc_optmc_alm_free (nlmpc_optmc_optimizer_t *_optimizer)
{
  nlmpc_optmc_optimizer_t *optimizer = _optimizer;
  nlmpc_optmc_alm_resources_t *resources = _optimizer -> resources;
  if (resources)
    {
      if (resources -> sub_optimizer.problem)
	{
	  resources -> sub_optimizer.optimizer_free
	    (& (resources -> sub_optimizer));
	}
      if (resources -> sub_solution_extras_const)
	free (resources -> sub_solution_extras_const);
      if (resources -> sub_solution_extras_var)
	free (resources -> sub_solution_extras_var);
      if (resources -> sub_problem.parameters)
	free (resources -> sub_problem.parameters);
      if (resources -> multipliers.lambda_g)
	free (resources -> multipliers.lambda_g);
      if (resources -> multipliers.lambda_h)
	free (resources -> multipliers.lambda_h);
      if (resources -> h_prev)
	free (resources -> h_prev);
      if (resources -> g_prev)
	free (resources -> g_prev);
      if (resources -> inputs_prev)
	free (resources -> inputs_prev);
      if (resources -> states_prev)
	free (resources -> states_prev);
      optimizer -> problem -> solution_dstr (& (resources -> solution_next));
      free (resources);
    }
  optimizer -> problem = NULL;
  optimizer -> resources = NULL;
}
