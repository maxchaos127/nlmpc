
#include "nlmpc/optmc_alm_internals.h"

void
alm_sub_simulator (nlmpc_optmunc_solution_t *solution,
		   const unsigned int steps_init)
{
  const nlmpc_optmunc_problem_t *problem = solution -> problem;
  const alm_sub_parameters_t *parameters = problem -> parameters;
  const nlmpc_optmc_problem_t *super_problem = parameters -> super_problem;
  alm_sub_extras_var_t *extras_var = solution -> extras_var;
  alm_sub_extras_const_t *extras_const = solution -> extras_const;
  nlmpc_optmc_solution_t super_solution = {
    .problem = (nlmpc_optmc_problem_t*) super_problem,
    .states = solution -> states,
    .inputs = solution -> inputs,
    .cost = extras_var -> cost,
    .g = extras_var -> g,
    .h = extras_var -> h,
    .extras_var = extras_var -> super_extras_var,
    .extras_const = extras_const -> super_extras_const
  };
  super_problem -> simulator (& super_solution, steps_init);
  super_problem -> costfunc (& super_solution);
  extras_var -> cost = super_solution.cost;
  super_problem -> gfunc (& super_solution);
  super_problem -> hfunc (& super_solution);
}
