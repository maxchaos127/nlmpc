
#include "nlmpc/optmc_alm_internals.h"
#include <stdlib.h>
#include <string.h>
#include <math.h>

/*** optimizer:
 ***    TODO 
 *** solution:
 ***    The initial members of this argument passed to the function
 ***    will be used as the initial estimation of the solution
 ***    and therefore they must be consistent.
 ***    Also, the members of the best solution that will be returned
 ***    will be stored to this argument.
 ***/
void
nlmpc_optmc_alm_solver (nlmpc_optmc_optimizer_t *optimizer,
			nlmpc_optmc_solution_t *solution,
			nlmpc_optmc_optimizer_status_t *status)
{
  unsigned int kkk;
  const nlmpc_optmc_problem_t *problem = optimizer -> problem;
  const nlmpc_optmc_alm_options_t *options = optimizer -> options;
  nlmpc_optmc_alm_resources_t *resources = optimizer -> resources;
  const unsigned int states_amount = problem -> states_amount;
  const unsigned int inputs_amount = problem -> inputs_amount;
  const unsigned int g_amount = problem -> g_amount;
  const unsigned int h_amount = problem -> h_amount;
  /* nlmpc_optmc_alm_parameters_t *parameters = resources -> parameters; */
  nlmpc_optmc_alm_multipliers_t *multipliers = & (resources -> multipliers);
  double r = multipliers -> r;
  double *lambda_g = multipliers -> lambda_g;
  double *lambda_h = multipliers -> lambda_h;
  nlmpc_optmc_solution_t *solution_next = & (resources -> solution_next);
  /* nlmpc_optmc_solution_t _solution; */
  double *states_prev = resources -> states_prev;
  double *inputs_prev = resources -> inputs_prev;;
  double *g_prev = resources -> g_prev;
  double *h_prev = resources -> h_prev;
  /* double *states_next, *inputs_next, *g_next, *h_next; */
  double *states_next = solution_next -> states;
  double *inputs_next = solution_next -> inputs;
  double *g_next = solution_next -> g;
  double *h_next = solution_next -> h;
  int iter_flag;
  unsigned int iter_counter;
  nlmpc_optmunc_optimizer_t *sub_optimizer =
    & (resources -> sub_optimizer);
  nlmpc_optmunc_optimizer_status_t *sub_optimizer_status =
    & (resources -> sub_optimizer_status);
  nlmpc_optmunc_problem_t *sub_problem =
    & (resources -> sub_problem);
  nlmpc_optmunc_solution_t sub_solution;
  alm_sub_extras_var_t *sub_solution_extras_var;
  alm_sub_extras_const_t *sub_solution_extras_const;
  const double r_value_min = options -> r_value_min;
  const double r_value_max = options -> r_value_max;
  const double r_factor = options -> r_factor;
  const unsigned int iter_amount_max = options -> iter_amount_max;
  const double tol_states_abs = options -> tol_states_abs;
  const double tol_inputs_abs = options -> tol_inputs_abs;
  const double tol_g_abs = options -> tol_g_abs;
  const double tol_h_abs = options -> tol_h_abs;

  /*** Initialize status to an invalid state. ***/
  status -> flag_exit_status = -1;
  status -> flag_exit_code = -1;

  /*** Initialize next solution. ***/
  problem -> solution_copy (solution, solution_next);

  /*** Initialize previous solution. ***/
  /* for (kkk = 0; kkk < states_amount; ++kkk) { */
  /*   states_prev [kkk] = solution -> states [kkk]; */
  /* } */
  /* for (kkk = 0; kkk < inputs_amount; ++kkk) { */
  /*   inputs_prev [kkk] = solution -> inputs [kkk]; */
  /* } */
  /* for (kkk = 0; kkk < g_amount; ++kkk) { */
  /*   g_prev [kkk] = solution -> g [kkk]; */
  /* } */
  /* for (kkk = 0; kkk < h_amount; ++kkk) { */
  /*   h_prev [kkk] = solution -> h [kkk]; */
  /* } */
  if (states_amount >= 0)
    memcpy (states_prev, states_next, states_amount * sizeof (double));
  if (inputs_amount >= 0)
    memcpy (inputs_prev, inputs_next, inputs_amount * sizeof (double));
  if (g_amount >= 0)
    memcpy (g_prev, g_next, g_amount * sizeof (double));
  if (h_amount >= 0)
    memcpy (h_prev, h_next, h_amount * sizeof (double));

  /*** Initialize alm's parameters. ***/
  /* multipliers -> r = r_value_min; */
  /* r = multipliers -> r; */
  r = r_value_min;
  multipliers -> r = r;
  for (kkk = 0; kkk < g_amount; ++ kkk)
    lambda_g [kkk] += 2.0 * r * g_prev [kkk];
  for (kkk = 0; kkk < h_amount; ++ kkk)
    if (h_prev [kkk] > (-0.5 * lambda_h [kkk] / r))
      lambda_h [kkk] += 2.0 * r * h_prev [kkk];
    else
      lambda_h [kkk] = 0.0;
  
  /*** Construct the sub_solution. ***/
  sub_solution.problem = sub_problem;
  sub_solution.states = solution_next -> states;
  sub_solution.inputs = solution_next -> inputs;
  sub_solution.cost = 0.0;
  sub_solution_extras_var = resources -> sub_solution_extras_var;
  sub_solution_extras_var -> cost = solution_next -> cost;
  sub_solution_extras_var -> g = solution_next -> g;
  sub_solution_extras_var -> h = solution_next -> h;
  sub_solution_extras_var -> super_extras_var =
    solution_next -> extras_var;
  sub_solution.extras_var = sub_solution_extras_var;
  sub_solution_extras_const = resources -> sub_solution_extras_const;
  sub_solution_extras_const -> super_extras_const =
    solution_next -> extras_const;
  sub_solution.extras_const = sub_solution_extras_const;
  sub_problem -> costfunc (& sub_solution);

  /*** Main loop. ***/
  iter_flag = 0;
  iter_counter = 0;
  while (! iter_flag)
    {
      /*** Increment counter. ***/
      iter_counter ++;
      /*** Check whether or not the maximum amount of iterations was reached.
       ***/
      if (iter_counter > iter_amount_max) {
	iter_flag = 2;
	break;
      }
      /*** Check whether or not the maximum amount of time has elapsed.
       ***/
      if (problem -> timeout (problem)) {
	iter_flag = 3;
	break;
      }
      /*** Optimize the augmented cost function. ***/
      sub_optimizer -> solver (sub_optimizer,
			       & sub_solution,
			       sub_optimizer_status);
      /*** Check if sub-optimizer failed. ***/
      if (! (sub_optimizer_status -> flag_exit_status)) {
	iter_flag = 4;
	break;
      }
      /*** Construct next solution. ***/
      solution_next -> states = sub_solution.states;
      solution_next -> inputs = sub_solution.inputs;
      sub_solution_extras_var =
	(alm_sub_extras_var_t*) sub_solution.extras_var;
      resources -> sub_solution_extras_var =
	sub_solution_extras_var;
      solution_next -> cost = sub_solution_extras_var -> cost;
      solution_next -> g = sub_solution_extras_var -> g;
      solution_next -> h = sub_solution_extras_var -> h;
      solution_next -> extras_var =
	sub_solution_extras_var -> super_extras_var;
      sub_solution_extras_const =
	(alm_sub_extras_const_t*) sub_solution.extras_const;
      resources -> sub_solution_extras_const =
	sub_solution_extras_const;
      solution_next -> extras_const =
	sub_solution_extras_const -> super_extras_const;
      /*** Get specific members of next solution. ***/
      states_next = solution_next -> states;
      inputs_next = solution_next -> inputs;
      g_next = solution_next -> g;
      h_next = solution_next -> h;
      /*** Check whether or not the states, inputs g and h
       *** have all converged to a solution.
       ***/
      iter_flag = 1;
      if (iter_flag)
	for (kkk = 0; kkk < states_amount; ++kkk)
	  if (fabs (states_next [kkk] - states_prev [kkk]) > tol_states_abs) {
	    iter_flag = 0;
	    break;
	  }
      if (iter_flag)
	for (kkk = 0; kkk < inputs_amount; ++kkk)
	  if (fabs (inputs_next [kkk] - inputs_prev [kkk]) > tol_inputs_abs) {
	    iter_flag = 0;
	    break;
	  }
      if (iter_flag)
	for (kkk = 0; kkk < g_amount; ++kkk)
	  if (fabs (g_next [kkk] - g_prev [kkk]) > tol_g_abs) {
	    iter_flag = 0;
	    break;
	  }
      if (iter_flag)
	for (kkk = 0; kkk < h_amount; ++kkk)
	  if (fabs (h_next [kkk] - h_prev [kkk]) > tol_h_abs) {
	    iter_flag = 0;
	    break;
	  }
      /*** Main loop's updating stage. ***/
      if (! iter_flag)
	{
	  /*** Calculate new r. ***/
	  r *= r_factor;
	  if (r > r_value_max)
	    r = r_value_max;
	  multipliers -> r = r;
	  /*** Calculate new lambda_g. ***/
	  for (kkk = 0; kkk < g_amount; ++kkk)
	    lambda_g [kkk] += 2.0 * r * g_next [kkk];
	  /*** Calculate new lambda_h. ***/
	  for (kkk = 0; kkk < h_amount; ++kkk)
	    if (h_next [kkk] > (-0.5 * lambda_h [kkk] / r))
	      lambda_h [kkk] += 2.0 * r * h_next [kkk];
	    else
	      lambda_h [kkk] = 0.0;
	  /*** Turn this iteration's next solution
	   *** into the next's iteration's previous solution.
	   ***/
	  /* for (kkk = 0; kkk < states_amount; ++kkk) */
	  /*   states_prev [kkk] = states_next [kkk]; */
	  /* for (kkk = 0; kkk < inputs_amount; ++kkk) */
	  /*   inputs_prev [kkk] = inputs_next [kkk]; */
	  /* for (kkk = 0; kkk < g_amount; ++kkk) */
	  /*   g_prev [kkk] = g_next [kkk]; */
	  /* for (kkk = 0; kkk < h_amount; ++kkk) */
	  /*   h_prev [kkk] = h_next [kkk]; */
	  memcpy (states_prev, states_next, states_amount * sizeof (double));
	  memcpy (inputs_prev, inputs_next, inputs_amount * sizeof (double));
	  if (g_amount)
	    memcpy (g_prev, g_next, g_amount * sizeof (double));
	  if (h_amount)
	    memcpy (h_prev, h_next, h_amount * sizeof (double));
	  /*** Make new sub_solution consistent
	   *** (since the r, lambda_g and lambda_h have been altered).
	   ***/
	  sub_problem -> costfunc (& sub_solution);
	}
    }

  if ((solution_next -> cost) > (solution -> cost))
    {
      /*** Failed to find a better solution. ***/
      status -> flag_exit_status = 0;
      status -> flag_exit_code = 0;
      /* return; */
    }
  else if ((solution_next -> cost) <= (solution -> cost))
    {
      if ((iter_flag == 2) || (iter_flag == 3) || (iter_flag == 4))
	{
	  /**** Either:
	   ****  * the algorithm maxed out its iterations,
	   ****  * the solution has been evaluated sufficient,
	   ****  * the sub-optimization of one of the iterations failed,
	   **** Either way,
	   **** the next solution is either the same or better than the initial,
	   **** so return it.
	   ***/
	  status -> flag_exit_status = 1;
	  status -> flag_exit_status = 0;
	}
      if (iter_flag == 1)
	{
	  /*** The solution found is the best possible solution.
	   ***/
	  status -> flag_exit_status = 2;
	  status -> flag_exit_code = 0;
	}
      /*** Return the best solution found. ***/
      /* _solution = *solution; */
      /* *solution = *solution_next; */
      /* *solution_next = _solution; */
      problem -> solution_copy (solution_next, solution);
    }
  else
    {
      /*** Something went really wrong. ***/
      status -> flag_exit_status = 0;
      status -> flag_exit_code = 1;
      /* return; */
    }
  return;
}
