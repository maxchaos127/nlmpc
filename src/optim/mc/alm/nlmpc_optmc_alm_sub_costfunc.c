
#include "nlmpc/optmc_alm_internals.h"

void
alm_sub_costfunc (nlmpc_optmunc_solution_t *solution)
{
  unsigned int kkk;
  const nlmpc_optmunc_problem_t *problem = solution -> problem;
  const alm_sub_parameters_t *parameters = problem -> parameters;
  const nlmpc_optmc_problem_t *super_problem = parameters -> super_problem;
  alm_sub_extras_var_t *extras_var = solution -> extras_var;
  alm_sub_extras_const_t *extras_const = solution -> extras_const;
  nlmpc_optmc_solution_t super_solution = {
    .states = solution -> states,
    .inputs = solution -> inputs,
    .cost = extras_var -> cost,
    .g = extras_var -> g,
    .h = extras_var -> h,
    .extras_var = extras_var -> super_extras_var,
    .extras_const = extras_const -> super_extras_const
  };
  const unsigned int g_amount = super_problem -> g_amount;
  const unsigned int h_amount = super_problem -> h_amount;
  /* const nlmpc_optmc_alm_parameters_t *parameters = parameters -> parameters; */
  const double *lambda_g = parameters -> multipliers -> lambda_g;
  const double *lambda_h = parameters -> multipliers -> lambda_h;
  const double r = parameters -> multipliers -> r;
  double alpha, alpha_a, alpha_b;
  double sub_cost = 0.0;
  /*** Calculate the augmented cost. ***/
  sub_cost = super_solution.cost;
  for (kkk = 0; kkk < g_amount; ++ kkk)
    sub_cost +=
      (lambda_g [kkk] + r * super_solution.g [kkk]) * super_solution.g [kkk];
  for (kkk = 0; kkk < h_amount; ++ kkk)
    {
      alpha_a = super_solution.h [kkk];
      alpha_b = -0.5 * lambda_h [kkk] / r;
      alpha = (alpha_a > alpha_b) ? alpha_a : alpha_b;
      sub_cost += (lambda_h [kkk] + r * alpha) * alpha;
    }
  /*** Return the augmented cost. ***/
  solution -> cost = sub_cost;
  return;
}
