
#include "nlmpc/opt_munc.h"

void
nlmpc_optmunc_costfunc_grad (const nlmpc_optmunc_solution_t *solution,
			     const double inputs_delta,
			     double *cost_grad,
			     nlmpc_optmunc_solution_t *solution_utls)
{
  int iii, jjj;
  const nlmpc_optmunc_problem_t *problem = solution -> problem;
  const unsigned int inputs_dim = problem -> inputs_dim;
  const unsigned int steps_amount = problem -> steps_amount;
  const unsigned int steps_init = problem -> steps_init;
  const unsigned int steps_final = problem -> steps_final;
  /* unsigned int steps_init = 0; */
  unsigned int idx;
  nlmpc_optmunc_solution_t *solution_perturbed;
  double cost, cost_perturbed;
  const double inputs_delta_inv = 1.0 / inputs_delta;
  /*** Complete copy of "solution" to "solution_perturbed" ***/
  solution_perturbed = solution_utls;
  problem -> solution_copy (solution, solution_utls);
  /*** Get the cost of reference solution ***/
  cost = solution -> cost;
  /**** Initialize gradient. ****/
  for (iii = 0; iii < steps_amount; ++ iii)
    for (jjj = 0; jjj < inputs_dim; ++ jjj)
      cost_grad [iii * inputs_dim + jjj] = 0.0;
  /**** Calculate the gradient. ****/
  for (iii = steps_final - 1; iii >= steps_init; --iii)
    {
      /* steps_init = iii; */
      for (jjj = 0; jjj < inputs_dim; ++jjj)
	{
	  idx = iii * inputs_dim + jjj;
	  solution_perturbed -> inputs [idx] += inputs_delta;
	  problem -> simulator (solution_perturbed, iii);
	  problem -> costfunc (solution_perturbed);
	  cost_perturbed = solution_perturbed -> cost;
	  cost_grad [idx] = (cost_perturbed - cost) * inputs_delta_inv;
	  solution_perturbed -> inputs [idx] -= inputs_delta;
	}
    }
}
