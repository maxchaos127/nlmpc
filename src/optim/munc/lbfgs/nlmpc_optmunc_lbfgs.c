
#include "nlmpc/optmunc_lbfgs_internals.h"
#include <stdlib.h>

int
nlmpc_optmunc_lbfgs_alloc (nlmpc_optmunc_optimizer_t *_optimizer,
			   nlmpc_optmunc_problem_t *_problem)
{
  int fs;
  int cstr_flag;
  nlmpc_optmunc_optimizer_t *optimizer = _optimizer;
  nlmpc_optmunc_problem_t *problem = _problem;
  nlmpc_optmunc_lbfgs_options_t *options = optimizer -> options;
  nlmpc_optmunc_lbfgs_resources_t *resources;
  unsigned int n, m;
  lbfgs_sub_parameters_t *sub_problem_parameters;
  lbfgs_sub_extras_var_t *sub_solution_extras_var;
  lbfgs_sub_extras_const_t *sub_solution_extras_const;
  /*** Set flag of allocations to success. ***/
  cstr_flag = 0;
  /*** Allocate resources. ***/
  n = problem -> inputs_amount;
  m = options -> history_depth;
  resources = NULL;
  if (! cstr_flag)
    {
      resources =
	(nlmpc_optmunc_lbfgs_resources_t*)
	malloc (sizeof (nlmpc_optmunc_lbfgs_resources_t));
      if (resources)
	{
	  resources -> solution_next.problem = NULL;
	  resources -> inputs_prev = NULL;
	  resources -> cost_grad_prev = NULL;
	  resources -> cost_grad_next = NULL;
	  resources -> indices = NULL;
	  resources -> Rho = NULL;
	  resources -> S = NULL;
	  resources -> T = NULL;
	  resources -> alpha = NULL;
	  resources -> q = NULL;
	  resources -> sub_optimizer.problem = NULL;
	  resources -> sub_problem.parameters = NULL;
	  resources -> sub_solution_extras_var = NULL;
	  resources -> sub_solution_extras_const = NULL;
	}
      else
	cstr_flag = 1;
    }
  if (! cstr_flag)
    {
      fs = problem -> solution_cstr (problem, & (resources -> solution_next));
      if (fs)
	cstr_flag = 2;
    }
  if ((! cstr_flag) && (problem -> inputs_amount))
    {
      resources -> inputs_prev = (double*) malloc (n * sizeof (double));
      if (! (resources -> inputs_prev))
	cstr_flag = 3;
    }
  if ((! cstr_flag) && (problem -> inputs_amount))
    {
      resources -> cost_grad_prev = (double*) malloc (n * sizeof (double));
      if (! (resources -> cost_grad_prev))
	cstr_flag = 4;
    }
  if ((! cstr_flag) && (problem -> inputs_amount))
    {
      resources -> cost_grad_next = (double*) malloc (n * sizeof (double));
      if (! (resources -> cost_grad_next))
	cstr_flag = 5;
    }
  if ((! cstr_flag) && (options -> history_depth))
    {
      resources -> indices =
	(unsigned int*) malloc (m * sizeof (unsigned int));
      if (! (resources -> indices))
	cstr_flag = 6;
    }
  if ((! cstr_flag) && (options -> history_depth))
    {
      resources -> Rho = (double*) malloc (m * sizeof (double));
      if (! (resources -> Rho))
	cstr_flag = 7;
    }
  if ((! cstr_flag) &&
      (problem -> inputs_amount) && (options -> history_depth))
    {
      resources -> S = (double*) malloc (n * m * sizeof (double));
      if (! (resources -> S))
	cstr_flag = 8;
    }
  if ((! cstr_flag) &&
      (problem -> inputs_amount) && (options -> history_depth))
    {
      resources -> T = (double*) malloc (n * m * sizeof (double));
      if (! (resources -> T))
	cstr_flag = 9;
    }
  if ((! cstr_flag) && (options -> history_depth))
    {
      resources -> alpha = (double*) malloc (m * sizeof (double));
      if (! (resources -> alpha))
	cstr_flag = 10;
    }
  if ((! cstr_flag) && (problem -> inputs_amount))
    {
      resources -> q = (double*) malloc (n * sizeof (double));
      if (! (resources -> q))
	cstr_flag = 11;
    }
  if (! cstr_flag)
    {
      fs = problem -> solution_cstr (problem, & (resources -> solution_utls));
      if (fs)
	cstr_flag = 12;
    }
  if (! cstr_flag)
    {
      sub_problem_parameters =
	(lbfgs_sub_parameters_t*) malloc (sizeof (lbfgs_sub_parameters_t));
      resources -> sub_problem.parameters = sub_problem_parameters;
      if (! sub_problem_parameters)
	cstr_flag = 13;
    }
  if (! cstr_flag)
    {
      sub_solution_extras_var =
	(lbfgs_sub_extras_var_t*) malloc (sizeof (lbfgs_sub_extras_var_t));
      resources -> sub_solution_extras_var = sub_solution_extras_var;
      if (! sub_solution_extras_var)
	cstr_flag = 14;
    }
  if (! cstr_flag)
    {
      sub_solution_extras_const =
	(lbfgs_sub_extras_const_t*) malloc (sizeof (lbfgs_sub_extras_const_t));
      resources -> sub_solution_extras_const = sub_solution_extras_const;
      if (! sub_solution_extras_const)
	cstr_flag = 15;
    }
  /* if (! cstr_flag) */
  /*   { */
  /*     resources -> sub_system.step_duration = system -> step_duration; */
  /*     resources -> sub_system.states_dim = system -> states_dim; */
  /*     resources -> sub_system.inputs_dim = system -> inputs_dim; */
  /*     resources -> sub_system.states_init = system -> states_init; */
  /*     resources -> sub_system.simulator = lbfgs_sub_simulator; */
  /*   } */
  if (! cstr_flag)
    {
      resources -> sub_problem.steps_amount = problem -> steps_amount;
      resources -> sub_problem.steps_init = problem -> steps_init;
      resources -> sub_problem.steps_final = problem -> steps_final;
      resources -> sub_problem.states_dim = problem -> states_dim;
      resources -> sub_problem.inputs_dim = problem -> inputs_dim;
      resources -> sub_problem.states_amount = problem -> states_amount;
      resources -> sub_problem.inputs_amount = problem -> inputs_amount;
      resources -> sub_problem.costfunc = lbfgs_sub_costfunc;
      resources -> sub_problem.simulator = lbfgs_sub_simulator;
      resources -> sub_problem.solution_cstr = lbfgs_sub_solution_cstr;
      resources -> sub_problem.solution_dstr = lbfgs_sub_solution_dstr;
      /* resources -> sub_problem.solution_init = lbfgs_sub_solution_init; */
      resources -> sub_problem.solution_copy = lbfgs_sub_solution_copy;
      /* resources -> sub_problem.solution_eval = lbfgs_sub_solution_eval; */
      resources -> sub_problem.timeout = lbfgs_sub_timeout;
      resources -> sub_problem.direction = resources -> q;
      sub_problem_parameters -> super_problem = problem;
    }
  if (! cstr_flag)
    {
      resources -> sub_optimizer.problem = NULL;
      resources -> sub_optimizer.solver = options -> sub_optimizer_solver;
      resources -> sub_optimizer.options = options -> sub_optimizer_options;
      resources -> sub_optimizer.resources = NULL;
      resources -> sub_optimizer.optimizer_alloc =
	options -> sub_optimizer_alloc;
      resources -> sub_optimizer.optimizer_free =
	options -> sub_optimizer_free;
      fs = resources -> sub_optimizer.optimizer_alloc
	(& (resources -> sub_optimizer), & (resources -> sub_problem));
      if (fs)
	cstr_flag = 16;
    }
  if (! cstr_flag)
    {
      resources -> sub_optimizer_status.flag_exit_status = -1;
      resources -> sub_optimizer_status.flag_exit_code = -1;
    }
  if (cstr_flag)
    {
      if (resources)
	{
	  if (resources -> sub_optimizer.problem)
	    {
	      resources -> sub_optimizer.optimizer_free
		(& (resources -> sub_optimizer));
	    }
	  if (resources -> sub_solution_extras_const)
	    free (resources -> sub_solution_extras_const);
	  if (resources -> sub_solution_extras_var)
	    free (resources -> sub_solution_extras_var);
	  if (resources -> sub_problem.parameters)
	    free (resources -> sub_problem.parameters);
	  problem -> solution_dstr (& (resources -> solution_utls));
	  if (resources -> q)
	    free (resources -> q);
	  if (resources -> alpha)
	    free (resources -> alpha);
	  if (resources -> T)
	    free (resources -> T);
	  if (resources -> S)
	    free (resources -> S);
	  if (resources -> Rho)
	    free (resources -> Rho);
	  if (resources -> indices)
	    free (resources -> indices);
	  if (resources -> cost_grad_next)
	    free (resources -> cost_grad_next);
	  if (resources -> cost_grad_prev)
	    free (resources -> cost_grad_prev);
	  if (resources -> inputs_prev)
	    free (resources -> inputs_prev);
	  problem -> solution_dstr (& (resources -> solution_next));
	  free (resources);
	}
      return cstr_flag;
    }
  optimizer -> problem = problem;
  optimizer -> resources = resources;
  return 0;
}

void
nlmpc_optmunc_lbfgs_free (nlmpc_optmunc_optimizer_t *_optimizer)
{
  nlmpc_optmunc_optimizer_t *optimizer = _optimizer;
  nlmpc_optmunc_lbfgs_resources_t *resources = optimizer -> resources;
  if (resources)
    {
      if (resources -> sub_optimizer.problem)
	{
	  resources -> sub_optimizer.optimizer_free
	    (& (resources -> sub_optimizer));
	}
      if (resources -> sub_solution_extras_const)
	free (resources -> sub_solution_extras_const);
      if (resources -> sub_solution_extras_var)
	free (resources -> sub_solution_extras_var);
      if (resources -> sub_problem.parameters)
	free (resources -> sub_problem.parameters);
      optimizer -> problem -> solution_dstr (& (resources -> solution_utls));
      if (resources -> q)
	free (resources -> q);
      if (resources -> alpha)
	free (resources -> alpha);
      if (resources -> T)
	free (resources -> T);
      if (resources -> S)
	free (resources -> S);
      if (resources -> Rho)
	free (resources -> Rho);
      if (resources -> indices)
	free (resources -> indices);
      if (resources -> cost_grad_next)
	free (resources -> cost_grad_next);
      if (resources -> cost_grad_prev)
	free (resources -> cost_grad_prev);
      if (resources -> inputs_prev)
	free (resources -> inputs_prev);
      optimizer -> problem -> solution_dstr (& (resources -> solution_next));
      free (resources);
    }
  optimizer -> problem = NULL;
  optimizer -> resources = NULL;
}
