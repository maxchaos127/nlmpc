
#include "nlmpc/optmunc_lbfgs_internals.h"

void
lbfgs_sub_simulator (nlmpc_optsunc_solution_t *solution,
		     const unsigned int steps_init)
{
  const nlmpc_optsunc_problem_t *problem = solution -> problem;
  const lbfgs_sub_parameters_t *parameters = problem -> parameters;
  const nlmpc_optmunc_problem_t *super_problem = parameters -> super_problem;
  lbfgs_sub_extras_var_t *extras_var = solution -> extras_var;
  lbfgs_sub_extras_const_t *extras_const = solution -> extras_const;
  nlmpc_optmunc_solution_t super_solution = {
    .problem = (nlmpc_optmunc_problem_t*) super_problem,
    .states = solution -> states,
    .inputs = solution -> inputs,
    .cost = solution -> cost,
    .extras_var = extras_var -> super_extras_var,
    .extras_const = extras_const -> super_extras_const
  };
  super_problem -> simulator (& super_solution, steps_init);
}

