
#include "nlmpc/optmunc_lbfgs_internals.h"
#include <stdlib.h>
#include <string.h>
#include <math.h>

void
nlmpc_optmunc_lbfgs_solver (nlmpc_optmunc_optimizer_t *optimizer,
			    nlmpc_optmunc_solution_t *solution,
			    nlmpc_optmunc_optimizer_status_t *status)
{
  unsigned int iii, jjj, kkk;
  const nlmpc_optmunc_problem_t *problem = optimizer -> problem;
  const nlmpc_optmunc_lbfgs_options_t *options = optimizer -> options;
  nlmpc_optmunc_lbfgs_resources_t *resources = optimizer -> resources;
  const unsigned int n = problem -> inputs_amount;
  const unsigned int m = options -> history_depth;
  double q_norm, q_elem_norm;
  unsigned int idx, idx_pivot;
  unsigned int *indices = resources -> indices;
  unsigned int indices_amount;
  double gamma;
  double *Rho = resources -> Rho, *S = resources -> S, *T = resources -> T;
  double *alpha = resources -> alpha;
  double *q = resources -> q;
  double p;
  nlmpc_optmunc_solution_t *solution_next = & (resources -> solution_next);
  nlmpc_optmunc_solution_t *solution_utls = & (resources -> solution_utls);
  /* nlmpc_optmunc_solution_t _solution; */
  double cost_prev, cost_next;
  double *inputs_prev = resources -> inputs_prev;
  double *inputs_next = solution_next -> inputs;
  double *cost_grad_prev = resources -> cost_grad_prev;
  double *cost_grad_next = resources -> cost_grad_next;
  double *_cost_grad;
  int iter_flag;
  unsigned int iter_counter;
  nlmpc_optsunc_optimizer_t *sub_optimizer =
    & (resources -> sub_optimizer);
  nlmpc_optsunc_optimizer_status_t *sub_optimizer_status =
    & (resources -> sub_optimizer_status);
  nlmpc_optsunc_problem_t *sub_problem = 
    & (resources -> sub_problem);
  nlmpc_optsunc_solution_t sub_solution;
  lbfgs_sub_extras_var_t *sub_solution_extras_var;
  lbfgs_sub_extras_const_t *sub_solution_extras_const;
  const double inputs_delta = options -> inputs_delta;
  const unsigned int iter_amount_max = options -> iter_amount_max;
  const double tol_cost_abs = options -> tol_cost_abs;
  const double tol_inputs_abs = options -> tol_inputs_abs;

  /*** Initialize status to an invalid state. ***/
  status -> flag_exit_status = -1;
  status -> flag_exit_code = -1;

  /*** Initialize next solution. ***/
  problem -> solution_copy (solution, solution_next);

  /*** Initialize next inputs. ***/
  /* inputs_next = solution_next -> inputs; */

  /*** Initialize prev inputs, cost and cost_grad. ***/
  /* for (kkk = 0; kkk < n; ++kkk) { */
  /*   inputs_prev [kkk] = solution -> inputs [kkk]; */
  /* } */
  memcpy (inputs_prev, inputs_next, n * sizeof (double));
  cost_prev = solution -> cost;
  nlmpc_optmunc_costfunc_grad (solution_next,
			       inputs_delta,
			       cost_grad_prev, solution_utls);

  /*** Construct the sub_solution. ***/
  sub_solution.problem = sub_problem;
  sub_solution.states = solution_next -> states;
  sub_solution.inputs = solution_next -> inputs;
  sub_solution.cost = solution_next -> cost;
  sub_solution_extras_var = resources -> sub_solution_extras_var;
  sub_solution_extras_var -> super_extras_var =
    solution_next -> extras_var;
  sub_solution.extras_var = sub_solution_extras_var;
  sub_solution_extras_const = resources -> sub_solution_extras_const;
  sub_solution_extras_const -> super_extras_const =
    solution_next -> extras_const;
  sub_solution.extras_const = sub_solution_extras_const;

  /*** Main loop. ***/
  iter_flag = 0;
  iter_counter = 0;
  idx_pivot = -1;
  indices_amount = 0;
  gamma = options -> gamma_init;
  while (! iter_flag)
    {
      /*** Increment counter of iterations ***/
      iter_counter ++;
      /*** Check whether or not the maximum amount of iterations was reached.
       ***/
      if (iter_counter > iter_amount_max) {
	iter_flag = 2;
	break;
      }
      /*** Check whether or not the maximum amount of time has elapsed.
       ***/
      if (problem -> timeout (problem)) {
	iter_flag = 3;
	break;
      }
      /*** Calculate the steepest descent direction 'q'. ***/
      for (kkk = 0; kkk < n; kkk++)
	q [kkk] = gamma * cost_grad_prev [kkk];
      for (iii = 0; iii < indices_amount; ++iii)
	{
	  idx = indices [iii];
	  alpha [idx] = 0.0;
	  for (jjj = 0; jjj < n; ++jjj)
	    alpha [idx] += S [idx * n + jjj] * q [jjj];
	  alpha [idx] *= Rho [idx];
	  for (jjj = 0; jjj < n; ++jjj)
	    q [jjj] -= alpha [idx] * T [idx * n + jjj];
	}
      for (iii = indices_amount - 1; iii > -1; --iii)
	{
	  idx = indices [iii];
	  p = 0.0;
	  for (jjj = 0; jjj < n; ++jjj)
	    p += T [idx * n + jjj] * q [jjj];
	  p = alpha [idx] - Rho [idx] * p;
	  for (jjj = 0; jjj < n; ++jjj)
	    q [jjj] += p * S [idx * n + jjj];
	}
      /*** Normalize and negate the vector 'q'. ***/
      q_norm = 0.0;
      for (kkk = 0; kkk < n; ++kkk)
	{
	  q_elem_norm = fabs (q [kkk]);
	  if (q_norm < q_elem_norm)
	    q_norm = q_elem_norm;
	}
      if (q_norm > 0.0)
	{
	  q_norm = - 1.0 / q_norm;
	  for (kkk = 0; kkk < n; ++kkk)
	    q [kkk] *= q_norm;
	}
      /*** Minimize the cost function along the steepest direction. ***/
      sub_optimizer -> solver (sub_optimizer, & sub_solution,
			       sub_optimizer_status);
      /*** Check if sub-optimizer failed. ***/
      if (! (sub_optimizer_status -> flag_exit_status)) {
	iter_flag = 4;
	break;
      }
      /*** Construct next solution. ***/
      solution_next -> states = sub_solution.states;
      solution_next -> inputs = sub_solution.inputs;
      solution_next -> cost = sub_solution.cost;
      sub_solution_extras_var =
	(lbfgs_sub_extras_var_t*) sub_solution.extras_var;
      resources -> sub_solution_extras_var = sub_solution_extras_var;
      solution_next -> extras_var =
	sub_solution_extras_var -> super_extras_var;
      sub_solution_extras_const =
	(lbfgs_sub_extras_const_t*) sub_solution.extras_const;
      resources -> sub_solution_extras_const = sub_solution_extras_const;
      solution_next -> extras_const =
	sub_solution_extras_const -> super_extras_const;
      /*** Get specific members of next solution. ***/
      inputs_next = solution_next -> inputs;
      cost_next = solution_next -> cost;
      nlmpc_optmunc_costfunc_grad (solution_next,
				   inputs_delta,
				   cost_grad_next, solution_utls);
      /*** Update pivot index. ***/
      idx_pivot ++;
      if (idx_pivot >= m) idx_pivot = 0;
      /*** Update matrices "S", "T", and "Rho". ***/
      Rho [idx_pivot] = 0.0;
      for (kkk = 0; kkk < n; ++kkk) {
	S [idx_pivot * n + kkk] = inputs_next [kkk] - inputs_prev [kkk];
	T [idx_pivot * n + kkk] = cost_grad_next [kkk] - cost_grad_prev [kkk];
	Rho [idx_pivot] += T [idx_pivot * n + kkk] * S [idx_pivot * n + kkk];
      }
      Rho [idx_pivot] = 1.0 / Rho [idx_pivot];
      /*** Calculate history-indices of matrices Rho, S, T. ***/
      if (iter_counter < m + 1) indices_amount = iter_counter;
      for (kkk = 1; kkk < indices_amount; kkk++)
	indices [kkk] = indices [kkk-1];
      indices [0] = idx_pivot;
      /*** Assume that the algorithm has converged. ***/
      iter_flag = 1;
      /*** Verify the convergence
       *** by checking the error of the cost-function's gradient.
       ***/
      /* if (iter_flag) */
      /* 	{ */
      /* 	  for (kkk = 0; kkk < n; ++kkk) { */
      /* 	    if (fabs (T [idx_pivot * n + kkk]) > options -> tol_cost_grad_abs) { */
      /* 	      iter_flag = 0; */
      /* 	      break; */
      /* 	    } */
      /* 	  } */
      /* 	} */
      if (iter_flag)
	if (fabs (cost_next - cost_prev) > tol_cost_abs)
	  iter_flag = 0;
      if (iter_flag)
	for (kkk = 0; kkk < n; ++kkk)
	  if (fabs (inputs_prev [kkk] - inputs_next [kkk]) > tol_inputs_abs) {
	    iter_flag = 0;
	    break;
	  }
      /*** Main loop's updating stage. ***/
      if (! iter_flag)
	{
	  /* for (kkk = 0; kkk < n; ++kkk) { */
	  /*   inputs_prev [kkk] = inputs_next [kkk]; */
	  /* } */
	  memcpy (inputs_prev, inputs_next, n * sizeof (double));
	  _cost_grad = cost_grad_prev;
	  cost_grad_prev = cost_grad_next;
	  cost_grad_next = _cost_grad;
	  resources -> cost_grad_prev = cost_grad_prev;
	  resources -> cost_grad_next = cost_grad_next;
	}
    }

  if (cost_next > solution -> cost)
    {
      /*** Failed to find a better solution. ***/
      status -> flag_exit_status = 0;
      status -> flag_exit_code = 0;
      /* return; */
    }
  else if (cost_next <= solution -> cost)
    {
      if ((iter_flag == 2) || (iter_flag == 3) || (iter_flag == 4))
	{
	  /*** The solution found is just better than the initial solution.
	   ***/
	  status -> flag_exit_status = 1;
	  status -> flag_exit_code = 0;
	}
      else if (iter_flag == 1)
	{
	  /*** The solution found is the best possible solution,
	   *** given the initial solution passed.
	   ***/
	  status -> flag_exit_status = 2;
	  status -> flag_exit_code = 0;
	}
      /*** Return the best solution found. ***/
      /* _solution = *solution; */
      /* *solution = *solution_next; */
      /* *solution_next = _solution; */
      problem -> solution_copy (solution_next, solution);
    }
  else
    {
      /*** Something went really wrong. ***/
      status -> flag_exit_status = 0;
      status -> flag_exit_code = 1;
      /* return; */
    }
  return;
}
