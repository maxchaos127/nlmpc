
#include "nlmpc/optmunc_lbfgs_internals.h"
#include <stdlib.h>

int
lbfgs_sub_solution_cstr (const nlmpc_optsunc_problem_t *problem,
			 nlmpc_optsunc_solution_t *solution)
{
  int fs;
  int cstr_flag;
  const lbfgs_sub_parameters_t *parameters = problem -> parameters;
  const nlmpc_optmunc_problem_t *super_problem = parameters -> super_problem;
  lbfgs_sub_extras_var_t *extras_var = NULL;
  lbfgs_sub_extras_const_t *extras_const = NULL;
  nlmpc_optmunc_solution_t super_solution = {
    .problem = NULL,
    .states = NULL,
    .inputs = NULL,
    .extras_var = NULL,
    .extras_const = NULL
  };
  /**** Initialize solution.
   ****/
  solution -> problem = NULL;
  solution -> states = NULL;
  solution -> inputs = NULL;
  solution -> extras_var = NULL;
  solution -> extras_const = NULL;
  /*** Set flag of allocations to success. ***/
  cstr_flag = 0;
  /*** Allocate solution. ***/
  if (! cstr_flag)
    {
      fs = super_problem -> solution_cstr (super_problem, & super_solution);
      if (fs)
	cstr_flag = 1;
    }
  if (! cstr_flag)
    {
      extras_var =
	(lbfgs_sub_extras_var_t*) malloc (sizeof (lbfgs_sub_extras_var_t));
      if (! extras_var)
	cstr_flag = 2;
    }
  if (! cstr_flag)
    {
      extras_const =
	(lbfgs_sub_extras_const_t*) malloc (sizeof (lbfgs_sub_extras_const_t));
      if (! extras_const)
	cstr_flag = 3;
    }
  /*** Check whether or not the allocations where successful.
   *** If false, then deallocate everything and return failure.
   *** If true, then construct the solution and return success.
   ***/
  if (cstr_flag)
    {
      if (extras_const)
	free (extras_const);
      if (extras_var)
	free (extras_var);
      super_problem -> solution_dstr (& super_solution);
    }
  else
    {
      solution -> problem = (nlmpc_optsunc_problem_t*) problem;
      solution -> states = super_solution.states;
      solution -> inputs = super_solution.inputs;
      solution -> cost = 0.0;
      extras_var -> super_extras_var = super_solution.extras_var;
      solution -> extras_var = extras_var;
      extras_const -> super_extras_const = super_solution.extras_const;
      solution -> extras_const = extras_const;
    }
  return cstr_flag;
}

void
lbfgs_sub_solution_dstr (nlmpc_optsunc_solution_t *solution)
{
  const nlmpc_optsunc_problem_t *problem = NULL;
  const lbfgs_sub_parameters_t *parameters = NULL;
  const nlmpc_optmunc_problem_t *super_problem = NULL;
  lbfgs_sub_extras_var_t *extras_var = solution -> extras_var;
  lbfgs_sub_extras_const_t *extras_const = solution -> extras_const;
  void *super_extras_var = NULL;
  void *super_extras_const = NULL;
  nlmpc_optmunc_solution_t super_solution;
  if (! (solution -> problem))
    return;
  problem = solution -> problem;
  parameters = (lbfgs_sub_parameters_t*) (problem -> parameters);
  super_problem = (nlmpc_optmunc_problem_t*) (parameters -> super_problem);
  extras_var = (lbfgs_sub_extras_var_t*) (solution -> extras_var);
  extras_const = (lbfgs_sub_extras_const_t*) (solution -> extras_const);
  super_extras_var = extras_var -> super_extras_var;
  super_extras_const = extras_const -> super_extras_const;
  super_solution = (nlmpc_optmunc_solution_t) {
    .problem = (nlmpc_optmunc_problem_t*) super_problem,
    .states = solution -> states,
    .inputs = solution -> inputs,
    .cost = solution -> cost,
    .extras_var = super_extras_var,
    .extras_const = super_extras_const
  };
  super_problem -> solution_dstr (& super_solution);
  extras_var -> super_extras_var = NULL;
  free (extras_var);
  extras_const -> super_extras_const = NULL;
  free (extras_const);
  solution -> problem = NULL;
  solution -> states = NULL;
  solution -> inputs = NULL;
  solution -> cost = 0.0;
  solution -> extras_var = NULL;
  solution -> extras_const = NULL;
}

void
lbfgs_sub_solution_copy (const nlmpc_optsunc_solution_t *source_solution,
			 nlmpc_optsunc_solution_t *target_solution)
{
  const nlmpc_optmunc_problem_t *super_problem = NULL;
  const nlmpc_optsunc_problem_t *source_problem = source_solution -> problem;
  const lbfgs_sub_parameters_t *source_parameters = source_problem -> parameters;
  const nlmpc_optmunc_problem_t *source_super_problem =
    source_parameters -> super_problem;
  const lbfgs_sub_extras_var_t *source_extras_var =
    source_solution -> extras_var;
  const lbfgs_sub_extras_const_t *source_extras_const =
    source_solution -> extras_const;
  const nlmpc_optsunc_problem_t *target_problem = target_solution -> problem;
  const lbfgs_sub_parameters_t *target_parameters = target_problem -> parameters;
  const nlmpc_optmunc_problem_t *target_super_problem =
    target_parameters -> super_problem;
  lbfgs_sub_extras_var_t *target_extras_var =
    target_solution -> extras_var;
  lbfgs_sub_extras_const_t *target_extras_const =
    target_solution -> extras_const;
  const nlmpc_optmunc_solution_t source_super_solution = {
    .problem = (nlmpc_optmunc_problem_t*) source_super_problem,
    .states = source_solution -> states,
    .inputs = source_solution -> inputs,
    .cost = source_solution -> cost,
    .extras_var = source_extras_var -> super_extras_var,
    .extras_const = source_extras_const -> super_extras_const
  };
  nlmpc_optmunc_solution_t target_super_solution = {
    .problem = (nlmpc_optmunc_problem_t*) target_super_problem,
    .states = target_solution -> states,
    .inputs = target_solution -> inputs,
    .cost = target_solution -> cost,
    .extras_var = target_extras_var -> super_extras_var,
    .extras_const = target_extras_const -> super_extras_const
  };
  super_problem = source_super_problem;
  super_problem -> solution_copy (& source_super_solution, & target_super_solution);
  target_solution -> cost = source_solution -> cost;
}

/* int */
/* lbfgs_sub_solution_eval (const nlmpc_optsunc_solution_t *solution) */
/* { */
/*   const nlmpc_optsunc_problem_t *problem = solution -> problem; */
/*   const lbfgs_sub_parameters_t *parameters = problem -> parameters; */
/*   const nlmpc_optmunc_problem_t *super_problem = parameters -> super_problem; */
/*   const lbfgs_sub_extras_var_t *extras_var = solution -> extras_var; */
/*   const lbfgs_sub_extras_const_t *extras_const = solution -> extras_const; */
/*   nlmpc_optmunc_solution_t super_solution = { */
/*     .problem = (nlmpc_optmunc_problem_t*) super_problem, */
/*     .states = solution -> states, */
/*     .inputs = solution -> inputs, */
/*     .cost = solution -> cost, */
/*     .extras_var = extras_var -> super_extras_var, */
/*     .extras_const = extras_const -> super_extras_const */
/*   }; */
/*   if (super_problem -> solution_eval) */
/*     return super_problem -> solution_eval (& super_solution); */
/*   else */
/*     return 0; */
/* } */

int
lbfgs_sub_timeout (const nlmpc_optsunc_problem_t *problem)
{
  const lbfgs_sub_parameters_t *parameters = problem -> parameters;
  const nlmpc_optmunc_problem_t *super_problem = parameters -> super_problem;
  return super_problem -> timeout (super_problem);
}
