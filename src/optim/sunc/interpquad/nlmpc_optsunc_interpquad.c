
#include "nlmpc/optsunc_interpquad_internals.h"
#include <stdlib.h>

int
nlmpc_optsunc_interpquad_alloc (nlmpc_optsunc_optimizer_t *_optimizer,
				nlmpc_optsunc_problem_t *_problem)
{
  int fs;
  int cstr_alloc;
  nlmpc_optsunc_optimizer_t *optimizer = _optimizer;
  nlmpc_optsunc_problem_t *problem = _problem;
  /* const nlmpc_optsunc_interpquad_options_t *options = optimizer -> options; */
  nlmpc_optsunc_interpquad_resources_t *resources = NULL;
  /*** Set flag of allocations to success. ***/
  cstr_alloc = 0;
  /*** Allocate resources. ***/
  if (! cstr_alloc)
    {
      resources =
	(nlmpc_optsunc_interpquad_resources_t*)
	malloc (sizeof (nlmpc_optsunc_interpquad_resources_t));
      if (resources)
	{
	  resources -> solution_next.problem = NULL;
	}
      else
	cstr_alloc = 1;
    }
  if (! cstr_alloc)
    {
      fs = problem -> solution_cstr (problem, & (resources -> solution_next));
      if (fs)
	cstr_alloc = 2;
    }
  /*** Check whether or not resources were allocated successfully.
   *** If not, free the allocated resources and exit.
   ***/
  if (cstr_alloc)
    {
      if (resources)
	{
	  problem -> solution_dstr (& (resources -> solution_next));
	  free (resources);
	}
      return cstr_alloc;
    }
  optimizer -> problem = problem;
  optimizer -> resources = resources;
  return 0;
}

void
nlmpc_optsunc_interpquad_free (nlmpc_optsunc_optimizer_t *_optimizer)
{
  nlmpc_optsunc_optimizer_t *optimizer = _optimizer;
  nlmpc_optsunc_interpquad_resources_t *resources = optimizer -> resources;
  if (resources)
    {
      optimizer -> problem -> solution_dstr (& (resources -> solution_next));
      free (resources);
    }
  optimizer -> problem = NULL;
  optimizer -> resources = NULL;
}
