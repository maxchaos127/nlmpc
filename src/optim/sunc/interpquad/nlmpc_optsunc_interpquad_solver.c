
#include "nlmpc/optsunc_interpquad.h"
#include <stdlib.h>
#include <math.h>

void
nlmpc_optsunc_interpquad_solver (nlmpc_optsunc_optimizer_t *optimizer,
				 nlmpc_optsunc_solution_t *solution,
				 nlmpc_optsunc_optimizer_status_t *status)
{
  int iii, jjj, kkk;
  const nlmpc_optsunc_problem_t *problem = optimizer -> problem;
  const nlmpc_optsunc_interpquad_options_t *options = optimizer -> options;
  nlmpc_optsunc_interpquad_resources_t *resources = optimizer -> resources;
  const unsigned int inputs_dim = problem -> inputs_dim;
  const unsigned int steps_init = problem -> steps_init;
  const unsigned int steps_final = problem -> steps_final;
  const double *direction = problem -> direction;
  const double *inputs_ref = solution -> inputs;
  const double *inputs_vector_lb = options -> inputs_vector_lb;
  const double *inputs_vector_ub = options -> inputs_vector_ub;
  nlmpc_optsunc_solution_t *solution_next = & (resources -> solution_next);
  double inputs_kkk, inputs_next_kkk;
  double s;
  const double x_init = options -> x_init;
  const double x_min = options -> x_min;
  const double x_max = options -> x_max;
  const double relaxation = options -> relaxation;
  const double s_init = options -> s_init;
  const double s_factor_inc = options -> s_factor_inc;
  const double s_factor_dec = options -> s_factor_dec;
  double x, xa, xb, xc, x_prev;
  double y, ya, yb, yc, y_prev;
  double a, b, c, denom, h;
  int iter_raw_flag, iter_fine_flag;
  unsigned int iter_raw_counter, iter_fine_counter;
  double error_dx_abs, error_dy_abs, error_approx_abs;
  const unsigned int iter_amount_max = options -> iter_amount_max;
  const double tol_dx_abs = options -> tol_dx_abs;
  const double tol_dy_abs = options -> tol_dy_abs;
  const double tol_approx_abs = options -> tol_approx_abs;

  /*** Initialize status to an invalid state. ***/
  status -> flag_exit_status = -1;
  status -> flag_exit_code = -1;

  /*** Initialize next solution. ***/
  problem -> solution_copy (solution, solution_next);

  /*** The raw loop (the first) finds a region containing a minimum. ***/
  iter_raw_flag = 0;
  iter_raw_counter = 0;
  /*** Initialize search step 's'. ***/
  s = s_init;
  /*** Calculate initial point 'xa' and 'ya'. ***/
  xa = x_init;
  /* for (kkk = 0; kkk < inputs_amount; ++kkk) */
  /*   { */
  /*     inputs_next_kkk = inputs_ref [kkk] + xa * direction [kkk]; */
  /*     if (inputs_next_kkk > inputs_trajectory_ub [kkk]) { */
  /* 	solution_next -> inputs [kkk] = inputs_trajectory_ub [kkk]; */
  /* 	continue; */
  /*     } */
  /*     if (inputs_next_kkk < inputs_trajectory_lb [kkk]) { */
  /* 	solution_next -> inputs [kkk] = inputs_trajectory_lb [kkk]; */
  /* 	continue; */
  /*     } */
  /*     solution_next -> inputs [kkk] = inputs_next_kkk; */
  /*   } */
  for (iii = steps_init; iii < steps_final; ++ iii) {
    for (jjj = 0; jjj < inputs_dim; ++ jjj) {
      kkk = iii * inputs_dim + jjj;
      inputs_next_kkk = inputs_ref [kkk] + xa * direction [kkk];
      if (inputs_next_kkk > inputs_vector_ub [jjj])
  	solution_next -> inputs [kkk] = inputs_vector_ub [jjj];
      else if (inputs_next_kkk < inputs_vector_lb [jjj])
  	solution_next -> inputs [kkk] = inputs_vector_lb [jjj];
      else
	solution_next -> inputs [kkk] = inputs_next_kkk;
    }
  }
  problem -> simulator (solution_next, steps_init);
  problem -> costfunc (solution_next);
  ya = solution_next -> cost;
  /*** Calculate 'x' and 'y'. ***/
  x = xa + s;
  /* for (kkk = 0; kkk < inputs_amount; ++kkk) */
  /*   { */
  /*     inputs_next_kkk = inputs_ref [kkk] + x * direction [kkk]; */
  /*     if (inputs_next_kkk > inputs_trajectory_ub [kkk]) { */
  /* 	solution_next -> inputs [kkk] = inputs_trajectory_ub [kkk]; */
  /* 	continue; */
  /*     } */
  /*     if (inputs_next_kkk < inputs_trajectory_lb [kkk]) { */
  /* 	solution_next -> inputs [kkk] = inputs_trajectory_lb [kkk]; */
  /* 	continue; */
  /*     } */
  /*     solution_next -> inputs [kkk] = inputs_next_kkk; */
  /*   } */
  for (iii = steps_init; iii < steps_final; ++ iii) {
    for (jjj = 0; jjj < inputs_dim; ++ jjj) {
      kkk = iii * inputs_dim + jjj;
      inputs_next_kkk = inputs_ref [kkk] + x * direction [kkk];
      if (inputs_next_kkk > inputs_vector_ub [jjj])
  	solution_next -> inputs [kkk] = inputs_vector_ub [jjj];
      else if (inputs_next_kkk < inputs_vector_lb [jjj])
  	solution_next -> inputs [kkk] = inputs_vector_lb [jjj];
      else
	solution_next -> inputs [kkk] = inputs_next_kkk;
    }
  }
  problem -> simulator (solution_next, steps_init);
  problem -> costfunc (solution_next);
  y = solution_next -> cost;
  /**** Initialize optimizer's solution xb, yb. ****/
  xb = x; yb = y;
  while (! iter_raw_flag)
    {
      iter_raw_counter ++;
      if (iter_raw_counter > iter_amount_max) {
	iter_raw_flag = 2;
	break;
      }
      if (problem -> timeout (problem)) {
	iter_raw_flag = 3;
	break;
      }
      if (y >= ya)
	{
	  /* s *= 0.5; */
	  s *= s_factor_dec;
	  xc = x; yc = y;
	  x = xa + s;
	  /* for (kkk = 0; kkk < inputs_amount; ++kkk) */
	  /*   { */
	  /*     inputs_next_kkk = inputs_ref [kkk] + x * direction [kkk]; */
	  /*     if (inputs_next_kkk > inputs_trajectory_ub [kkk]) { */
	  /* 	solution_next -> inputs [kkk] = inputs_trajectory_ub [kkk]; */
	  /* 	continue; */
	  /*     } */
	  /*     if (inputs_next_kkk < inputs_trajectory_lb [kkk]) { */
	  /* 	solution_next -> inputs [kkk] = inputs_trajectory_lb [kkk]; */
	  /* 	continue; */
	  /*     } */
	  /*     solution_next -> inputs [kkk] = inputs_next_kkk; */
	  /*   } */
	  for (iii = steps_init; iii < steps_final; ++ iii) {
	    for (jjj = 0; jjj < inputs_dim; ++ jjj) {
	      kkk = iii * inputs_dim + jjj;
	      inputs_next_kkk = inputs_ref [kkk] + x * direction [kkk];
	      if (inputs_next_kkk > inputs_vector_ub [jjj])
		solution_next -> inputs [kkk] = inputs_vector_ub [jjj];
	      else if (inputs_next_kkk < inputs_vector_lb [jjj])
		solution_next -> inputs [kkk] = inputs_vector_lb [jjj];
	      else
		solution_next -> inputs [kkk] = inputs_next_kkk;
	    }
	  }
	  problem -> simulator (solution_next, steps_init);
	  problem -> costfunc (solution_next);
	  y = solution_next -> cost;
	  if (y <= yc)
	    {
	      xb = x; yb = y;
	      iter_raw_flag = 1;
	    }
	}
      else if (y < ya)
	{
	  xb = x; yb = y;
	  /* x = xa + 2.0 * s; */
	  x = xa + s_factor_inc * s;
	  /* for (kkk = 0; kkk < inputs_amount; ++kkk) */
	  /*   { */
	  /*     inputs_next_kkk = inputs_ref [kkk] + x * direction [kkk]; */
	  /*     if (inputs_next_kkk > inputs_trajectory_ub [kkk]) { */
	  /* 	solution_next -> inputs [kkk] = inputs_trajectory_ub [kkk]; */
	  /* 	continue; */
	  /*     } */
	  /*     if (inputs_next_kkk < inputs_trajectory_lb [kkk]) { */
	  /* 	solution_next -> inputs [kkk] = inputs_trajectory_lb [kkk]; */
	  /* 	continue; */
	  /*     } */
	  /*     solution_next -> inputs [kkk] = inputs_next_kkk; */
	  /*   } */
	  for (iii = steps_init; iii < steps_final; ++ iii) {
	    for (jjj = 0; jjj < inputs_dim; ++ jjj) {
	      kkk = iii * inputs_dim + jjj;
	      inputs_next_kkk = inputs_ref [kkk] + x * direction [kkk];
	      if (inputs_next_kkk > inputs_vector_ub [jjj])
		solution_next -> inputs [kkk] = inputs_vector_ub [jjj];
	      else if (inputs_next_kkk < inputs_vector_lb [jjj])
		solution_next -> inputs [kkk] = inputs_vector_lb [jjj];
	      else
		solution_next -> inputs [kkk] = inputs_next_kkk;
	    }
	  }
	  problem -> simulator (solution_next, steps_init);
	  problem -> costfunc (solution_next);
	  y = solution_next -> cost;
	  if (y >= yb)
	    {
	      xc = x; yc = y;
	      iter_raw_flag = 1;
	    }
	  else if (y < yb)
	    {
	      /* s *= 2.0; */
	      s *= s_factor_inc;
	    }
	}
      else
	{
	  /*** Normally unreachable branch.
	   *** If this branch is reached, then something went NaN.
	   ***/
	  s *= s_factor_dec;
	  x = xa + s;
	  /* for (kkk = 0; kkk < inputs_amount; ++kkk) */
	  /*   { */
	  /*     inputs_next_kkk = inputs_ref [kkk] + x * direction [kkk]; */
	  /*     if (inputs_next_kkk > inputs_trajectory_ub [kkk]) { */
	  /* 	solution_next -> inputs [kkk] = inputs_trajectory_ub [kkk]; */
	  /* 	continue; */
	  /*     } */
	  /*     if (inputs_next_kkk < inputs_trajectory_lb [kkk]) { */
	  /* 	solution_next -> inputs [kkk] = inputs_trajectory_lb [kkk]; */
	  /* 	continue; */
	  /*     } */
	  /*     solution_next -> inputs [kkk] = inputs_next_kkk; */
	  /*   } */
	  for (iii = steps_init; iii < steps_final; ++ iii) {
	    for (jjj = 0; jjj < inputs_dim; ++ jjj) {
	      kkk = iii * inputs_dim + jjj;
	      inputs_next_kkk = inputs_ref [kkk] + x * direction [kkk];
	      if (inputs_next_kkk > inputs_vector_ub [jjj])
		solution_next -> inputs [kkk] = inputs_vector_ub [jjj];
	      else if (inputs_next_kkk < inputs_vector_lb [jjj])
		solution_next -> inputs [kkk] = inputs_vector_lb [jjj];
	      else
		solution_next -> inputs [kkk] = inputs_next_kkk;
	    }
	  }
	  problem -> simulator (solution_next, steps_init);
	  problem -> costfunc (solution_next);
	  y = solution_next -> cost;
	}
    }

  /*** The fine loop (the second) locates the minimum within the region
   *** found by the raw loop with the desired precision.
   ***/
  iter_fine_flag = 0;
  iter_fine_counter = 0;
  x_prev = x; y_prev = y;
  if (iter_raw_flag == 2) iter_fine_flag = 2;
  if (iter_raw_flag == 3) iter_fine_flag = 3;
  while (! iter_fine_flag)
    {
      ++iter_fine_counter;
      if (iter_fine_counter + iter_raw_counter > iter_amount_max) {
	iter_fine_flag = 2;
	break;
      }
      if (problem -> timeout (problem)) {
	iter_fine_flag = 3;
	break;
      }
      denom = (xa - xb) * (xb - xc) * (xc - xa);
      a = ya*xb*xc*(xc-xb) + yb*xc*xa*(xa-xc) + yc*xa*xb*(xb-xa);
      b = ya*(xb*xb-xc*xc) + yb*(xc*xc-xa*xa) + yc*(xa*xa-xb*xb);
      c = ya*(xc-xb) + yb*(xa-xc) + yc*(xb-xa);
      /*** Check whether or not b and c are not zero. ***/
      /***
       *** TODO
       ***/
      /*** Calculate next solution 'x' and 'y'. ***/
      x = - 0.5 * b / c;
      if ((x < x_max) && (x > x_min)) {
	x = x_prev + relaxation * (x - x_prev);
      } else {
	x = 0.25 * (xa + 2.0 * xb + xc);
      }
      /* if (x < 0.0) { */
      /* 	x = (xa + xb + xc) / 3.0; */
      /* } */
      /* for (kkk = 0; kkk < inputs_amount; ++kkk) */
      /* 	{ */
      /* 	  inputs_next_kkk = inputs_ref [kkk] + x * direction [kkk]; */
      /* 	  if (inputs_next_kkk > inputs_trajectory_ub [kkk]) { */
      /* 	    solution_next -> inputs [kkk] = inputs_trajectory_ub [kkk]; */
      /* 	    continue; */
      /* 	  } */
      /* 	  if (inputs_next_kkk < inputs_trajectory_lb [kkk]) { */
      /* 	    solution_next -> inputs [kkk] = inputs_trajectory_lb [kkk]; */
      /* 	    continue; */
      /* 	  } */
      /* 	  solution_next -> inputs [kkk] = inputs_next_kkk; */
      /* 	} */
      for (iii = steps_init; iii < steps_final; ++ iii) {
	for (jjj = 0; jjj < inputs_dim; ++ jjj) {
	  kkk = iii * inputs_dim + jjj;
	  inputs_next_kkk = inputs_ref [kkk] + x * direction [kkk];
	  if (inputs_next_kkk > inputs_vector_ub [jjj])
	    solution_next -> inputs [kkk] = inputs_vector_ub [jjj];
	  else if (inputs_next_kkk < inputs_vector_lb [jjj])
	    solution_next -> inputs [kkk] = inputs_vector_lb [jjj];
	  else
	    solution_next -> inputs [kkk] = inputs_next_kkk;
	}
      }
      problem -> simulator (solution_next, steps_init);
      problem -> costfunc (solution_next);
      y = solution_next -> cost;
      a /= denom; b /= denom; c /= denom;
      h = a + b * x + c * x * x;
      error_dx_abs = fabs (x - x_prev);
      error_dy_abs = fabs (y - y_prev);
      error_approx_abs = fabs (h - y);
      /*** Assume the algorithm has converged. ***/
      iter_fine_flag = 1;
      /*** Check whether or not
       *** the approximation h, cost y and direction's magnitude x
       *** have converged to a solution.
       ***/
      if (error_approx_abs > tol_approx_abs) {
      	iter_fine_flag = 0;
      }
      if (error_dy_abs > tol_dy_abs) {
	iter_fine_flag = 0;
      }
      if (error_dx_abs > tol_dx_abs) {
	iter_fine_flag = 0;
      }
      /*** Fine loopt's updating stage. ***/
      if ((x >= xb) && (y <= yb))
	{
	  xa = xb; ya = yb;
	  xb = x; yb = y;
	}
      else if ((x >= xb) && (y >= yb))
	{
	  xc = x; yc = y;
	}
      else if ((x <= xb) && (y <= yb))
	{
	  xc = xb; yc = yb;
	  xb = x; yb = y;
	}
      else if ((x <= xb) && (y >= yb))
	{
	  xa = x; ya = y;
	}
      else
	{
	  /*** Normally unreachable branch.
	   *** If this branch is reached, then something went NaN.
	   ***/
	  iter_fine_flag = 4;
	  break;
	}
      x_prev = x; y_prev = y;
    }

  if (yb >= 0.0)
    {
      if (yb > solution -> cost)
	{
	  /*** Failed to find a better solution. ***/
	  status -> flag_exit_status = 0;
	  status -> flag_exit_code = 0;
	  return;
	}
      else if (yb <= solution -> cost)
	{
	  if ((iter_raw_flag == 2) || (iter_raw_flag == 3) ||
	      (iter_fine_flag == 2) || (iter_fine_flag == 3)) {
	    /*** The solution found is just better than the initial solution.
	     ***/
	    status -> flag_exit_status = 1;
	    status -> flag_exit_code = 0;
	  } else if ((iter_raw_flag == 1) && (iter_fine_flag == 1)) {
	    /*** The solution found is the best possible solution,
	     *** given the initial solution passed.
	     ***/
	    status -> flag_exit_status = 2;
	    status -> flag_exit_code = 0;
	  } else {
	    /*** No solution could be found. ***/
	    status -> flag_exit_status = 0;
	    status -> flag_exit_status = 1;
	    return;
	  }
	  /*** Return the best solution found. ***/
	  /* for (kkk = 0; kkk < inputs_amount; ++kkk) */
	  /*   { */
	  /*     inputs_kkk = inputs_ref [kkk] + xb * direction [kkk]; */
	  /*     if (inputs_kkk > inputs_trajectory_ub [kkk]) { */
	  /* 	solution -> inputs [kkk] = inputs_trajectory_ub [kkk]; */
	  /* 	continue; */
	  /*     } */
	  /*     if (inputs_kkk < inputs_trajectory_lb [kkk]) { */
	  /* 	solution -> inputs [kkk] = inputs_trajectory_lb [kkk]; */
	  /* 	continue; */
	  /*     } */
	  /*     solution -> inputs [kkk] = inputs_kkk; */
	  /*   } */
	  for (iii = steps_init; iii < steps_final; ++ iii) {
	    for (jjj = 0; jjj < inputs_dim; ++ jjj) {
	      kkk = iii * inputs_dim + jjj;
	      inputs_kkk = inputs_ref [kkk] + xb * direction [kkk];
	      if (inputs_kkk > inputs_vector_ub [jjj])
		solution -> inputs [kkk] = inputs_vector_ub [jjj];
	      else if (inputs_kkk < inputs_vector_lb [jjj])
		solution -> inputs [kkk] = inputs_vector_lb [jjj];
	      else
		solution -> inputs [kkk] = inputs_kkk;
	    }
	  }
	  problem -> simulator (solution, steps_init);
	  problem -> costfunc (solution);
	  return;
	}
    }
  else
    {
      /*** Something went really wrong
       *** (cost function should be non-negative).
       ***/
      status -> flag_exit_status = 0;
      status -> flag_exit_code = 2;
      return;
    }
  return;
}
