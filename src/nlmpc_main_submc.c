
#include "nlmpc/main_internals.h"
#include <stdlib.h>

void
submc_costfunc (nlmpc_optmc_solution_t *solution)
{
  const nlmpc_optmc_problem_t *problem = solution -> problem;
  const submc_parameters_t *parameters = problem -> parameters;
  const nlmpc_problem_t *super_problem = parameters -> super_problem;
  const submc_extras_var_t *extras_var = solution -> extras_var;
  const submc_extras_const_t *extras_const = solution -> extras_const;
  void *super_extras_var = extras_var -> super_extras_var;
  void *super_extras_const = extras_const -> super_extras_const;
  nlmpc_solution_t super_solution = {
    .problem = (nlmpc_problem_t*) super_problem,
    .steps_amount = problem -> steps_amount,
    .steps_init = problem -> steps_init,
    .steps_final = problem -> steps_final,
    .states_amount = problem -> states_amount,
    .inputs_amount = problem -> inputs_amount,
    .g_amount = problem -> g_amount,
    .h_amount = problem -> h_amount,
    .states = solution -> states,
    .inputs = solution -> inputs,
    .cost = solution -> cost,
    .g = solution -> g,
    .h = solution -> h,
    .extras_var = super_extras_var,
    .extras_const = super_extras_const
  };
  super_problem -> costfunc (& super_solution);
  solution -> cost = super_solution.cost;
}

void
submc_gfunc (nlmpc_optmc_solution_t *solution)
{
  const nlmpc_optmc_problem_t *problem = solution -> problem;
  const submc_parameters_t *parameters = problem -> parameters;
  const nlmpc_problem_t *super_problem = parameters -> super_problem;
  const submc_extras_var_t *extras_var = solution -> extras_var;
  const submc_extras_const_t *extras_const = solution -> extras_const;
  void *super_extras_var = extras_var -> super_extras_var;
  void *super_extras_const = extras_const -> super_extras_const;
  nlmpc_solution_t super_solution = {
    .problem = (nlmpc_problem_t*) super_problem,
    .steps_amount = problem -> steps_amount,
    .steps_init = problem -> steps_init,
    .steps_final = problem -> steps_final,
    .states_amount = problem -> states_amount,
    .inputs_amount = problem -> inputs_amount,
    .g_amount = problem -> g_amount,
    .h_amount = problem -> h_amount,
    .states = solution -> states,
    .inputs = solution -> inputs,
    .cost = solution -> cost,
    .g = solution -> g,
    .h = solution -> h,
    .extras_var = super_extras_var,
    .extras_const = super_extras_const
  };
  /* if (problem -> g_amount) { */
  /*   super_problem -> gfunc (problem -> steps_amount, */
  /* 			    solution -> states, solution -> inputs, */
  /* 			    super_extras_var, super_extras_const, */
  /* 			    solution -> g); */
  /* } */
  super_problem -> gfunc (& super_solution);
}

void
submc_hfunc (nlmpc_optmc_solution_t *solution)
{
  const nlmpc_optmc_problem_t *problem = solution -> problem;
  const submc_parameters_t *parameters = problem -> parameters;
  const nlmpc_problem_t *super_problem = parameters -> super_problem;
  const submc_extras_var_t *extras_var = solution -> extras_var;
  const submc_extras_const_t *extras_const = solution -> extras_const;
  void *super_extras_var = extras_var -> super_extras_var;
  void *super_extras_const = extras_const -> super_extras_const;
  nlmpc_solution_t super_solution = {
    .problem = (nlmpc_problem_t*) super_problem,
    .steps_amount = problem -> steps_amount,
    .steps_init = problem -> steps_init,
    .steps_final = problem -> steps_final,
    .states_amount = problem -> states_amount,
    .inputs_amount = problem -> inputs_amount,
    .g_amount = problem -> g_amount,
    .h_amount = problem -> h_amount,
    .states = solution -> states,
    .inputs = solution -> inputs,
    .cost = solution -> cost,
    .g = solution -> g,
    .h = solution -> h,
    .extras_var = super_extras_var,
    .extras_const = super_extras_const
  };
  /* if (problem -> h_amount) { */
  /*   super_problem -> hfunc (problem -> steps_amount, */
  /* 			    solution -> states, solution -> inputs, */
  /* 			    super_extras_var, super_extras_const, */
  /* 			    solution -> h); */
  /* } */
  super_problem -> hfunc (& super_solution);
}

void
submc_simulator (nlmpc_optmc_solution_t *solution,
		 const unsigned int steps_init)
{
  const nlmpc_optmc_problem_t *problem = solution -> problem;
  const submc_parameters_t *parameters = problem -> parameters;
  const nlmpc_problem_t *super_problem = parameters -> super_problem;
  const submc_extras_var_t *extras_var = solution -> extras_var;
  const submc_extras_const_t *extras_const = solution -> extras_const;
  void *super_extras_var = extras_var -> super_extras_var;
  void *super_extras_const = extras_const -> super_extras_const;
  nlmpc_solution_t super_solution = {
    .problem = (nlmpc_problem_t*) super_problem,
    .steps_amount = problem -> steps_amount,
    .steps_init = problem -> steps_init,
    .steps_final = problem -> steps_final,
    .states_amount = problem -> states_amount,
    .inputs_amount = problem -> inputs_amount,
    .g_amount = problem -> g_amount,
    .h_amount = problem -> h_amount,
    .states = solution -> states,
    .inputs = solution -> inputs,
    .cost = solution -> cost,
    .g = solution -> g,
    .h = solution -> h,
    .extras_var = super_extras_var,
    .extras_const = super_extras_const
  };
  /* super_problem -> simulator (problem -> steps_amount, */
  /* 			      step_init, step_final, */
  /* 			      solution -> inputs, super_extras_const, */
  /* 			      solution -> states, super_extras_var); */
  super_problem -> simulator (& super_solution, steps_init);
}

int
submc_solution_cstr (const nlmpc_optmc_problem_t *problem,
		     nlmpc_optmc_solution_t *solution)
{
  int fs;
  int cstr_flag;
  const submc_parameters_t *parameters = problem -> parameters;
  const nlmpc_problem_t *super_problem = parameters -> super_problem;
  submc_extras_var_t *extras_var = NULL;
  submc_extras_const_t *extras_const = NULL;
  nlmpc_solution_t super_solution = {
    .problem = NULL,
    .steps_amount = 0,
    .steps_init = 0,
    .steps_final = 0,
    .states_amount = 0,
    .inputs_amount = 0,
    .g_amount = 0,
    .h_amount = 0,
    .states = NULL,
    .inputs = NULL,
    .cost = 0,
    .g = NULL,
    .h = NULL,
    .extras_var = NULL,
    .extras_const = NULL
  };
  /* fs = super_problem -> solution_alloc */
  /*   (problem -> steps_amount, & super_solution, */
  /*    flags_states, flags_inputs, flags_g, flags_h, flags_extras_var, 0); */
  /**** Initialize solution.
   ****/
  solution -> problem = NULL;
  solution -> states = NULL;
  solution -> inputs = NULL;
  solution -> g = NULL;
  solution -> h = NULL;
  solution -> extras_var = NULL;
  solution -> extras_const = NULL;
  /**** Set cstr_flag to success.
   ****/
  cstr_flag = 0;
  /**** Construct solution.
   ****/
  if (! cstr_flag)
    {
      fs = super_problem -> solution_cstr (super_problem,
					   & super_solution,
					   problem -> steps_amount,
					   problem -> steps_init,
					   problem -> steps_final);
      if (fs)
	cstr_flag = 1;
    }
  if (! cstr_flag)
    {
      extras_var =
	(submc_extras_var_t*) malloc (sizeof (submc_extras_var_t));
      if (! extras_var)
	cstr_flag = 2;
    }
  if (! cstr_flag)
    {
      extras_const =
	(submc_extras_const_t*)	malloc (sizeof (submc_extras_const_t));
      if (! extras_const)
	cstr_flag = 3;
    }
  /**** Check if construction of solution successed.
   ****/
  if (cstr_flag)
    {
      if (extras_const)
	free (extras_const);
      if (extras_var)
	free (extras_var);
      super_problem -> solution_dstr (& super_solution);
    }
  else
    {
      solution -> problem = (nlmpc_optmc_problem_t*) problem;
      solution -> states = super_solution.states;
      solution -> inputs = super_solution.inputs;
      solution -> cost = super_solution.cost;
      solution -> g = super_solution.g;
      solution -> h = super_solution.h;
      extras_var -> super_extras_var = super_solution.extras_var;
      solution -> extras_var = extras_var;
      extras_const -> super_extras_const = super_solution.extras_const;
      solution -> extras_const = extras_const;
    }
  return cstr_flag;
}

void
submc_solution_dstr (nlmpc_optmc_solution_t *solution)
{
  const nlmpc_optmc_problem_t *problem = NULL;
  const submc_parameters_t *parameters = NULL;
  const nlmpc_problem_t *super_problem = NULL;
  submc_extras_var_t *extras_var = NULL;
  submc_extras_const_t *extras_const = NULL;
  void *super_extras_var = NULL;
  void *super_extras_const = NULL;
  nlmpc_solution_t super_solution;
  if (! (solution -> problem))
    return;
  problem = solution -> problem;
  parameters = (submc_parameters_t*) (problem -> parameters);
  super_problem = (nlmpc_problem_t*) (parameters -> super_problem);
  extras_var = (submc_extras_var_t*) (solution -> extras_var);
  extras_const = (submc_extras_const_t*) (solution -> extras_const);
  super_extras_var = extras_var -> super_extras_var;
  super_extras_const = extras_const -> super_extras_const;
  super_solution = (nlmpc_solution_t) {
    .problem = (nlmpc_problem_t*) super_problem,
    .steps_amount = problem -> steps_amount,
    .steps_init = problem -> steps_init,
    .steps_final = problem -> steps_final,
    .states_amount = problem -> states_amount,
    .inputs_amount = problem -> inputs_amount,
    .g_amount = problem -> g_amount,
    .h_amount = problem -> h_amount,
    .states = solution -> states,
    .inputs = solution -> inputs,
    .cost = solution -> cost,
    .g = solution -> g,
    .h = solution -> h,
    .extras_var = super_extras_var,
    .extras_const = super_extras_const
  };
  super_problem -> solution_dstr (& super_solution);
  extras_var -> super_extras_var = NULL;
  extras_const -> super_extras_const = NULL;
  free (extras_var);
  free (extras_const);
  solution -> problem = NULL;
  solution -> states = NULL;
  solution -> inputs = NULL;
  solution -> cost = 0.0;
  solution -> g = NULL;
  solution -> h = NULL;
  solution -> extras_var = NULL;
  solution -> extras_const = NULL;
}

void
submc_solution_copy (const nlmpc_optmc_solution_t *source_solution,
		     nlmpc_optmc_solution_t *target_solution)
{
  const nlmpc_problem_t *super_problem = NULL;
  const nlmpc_optmc_problem_t *source_problem =
    source_solution -> problem;
  const submc_parameters_t *source_parameters =
    source_problem -> parameters;
  const nlmpc_problem_t *source_super_problem =
    source_parameters -> super_problem;
  const submc_extras_var_t *source_extras_var =
    source_solution -> extras_var;
  const submc_extras_const_t *source_extras_const =
    source_solution -> extras_const;
  void *source_super_extras_var = source_extras_var -> super_extras_var;
  void *source_super_extras_const = source_extras_const -> super_extras_const;
  const nlmpc_optmc_problem_t *target_problem =
    target_solution -> problem;
  const submc_parameters_t *target_parameters =
    target_problem -> parameters;
  const nlmpc_problem_t *target_super_problem =
    target_parameters -> super_problem;
  const submc_extras_var_t *target_extras_var =
    target_solution -> extras_var;
  const submc_extras_const_t *target_extras_const =
    target_solution -> extras_const;
  void *target_super_extras_var = target_extras_var -> super_extras_var;
  void *target_super_extras_const = target_extras_const -> super_extras_const;
  const nlmpc_solution_t source_super_solution = {
    .problem = (nlmpc_problem_t*) source_super_problem,
    .steps_amount = source_problem -> steps_amount,
    .steps_init = source_problem -> steps_init,
    .steps_final = source_problem -> steps_final,
    .states_amount = source_problem -> states_amount,
    .inputs_amount = source_problem -> inputs_amount,
    .g_amount = source_problem -> g_amount,
    .h_amount = source_problem -> h_amount,
    .states = source_solution -> states,
    .inputs = source_solution -> inputs,
    .cost = source_solution -> cost,
    .g = source_solution -> g,
    .h = source_solution -> h,
    .extras_var = source_super_extras_var,
    .extras_const = source_super_extras_const
  };
  nlmpc_solution_t target_super_solution = {
    .problem = (nlmpc_problem_t*) target_super_problem,
    .steps_amount = target_problem -> steps_amount,
    .steps_init = target_problem -> steps_init,
    .steps_final = target_problem -> steps_final,
    .states_amount = target_problem -> states_amount,
    .inputs_amount = target_problem -> inputs_amount,
    .g_amount = target_problem -> g_amount,
    .h_amount = target_problem -> h_amount,
    .states = target_solution -> states,
    .inputs = target_solution -> inputs,
    .cost = target_solution -> cost,
    .g = target_solution -> g,
    .h = target_solution -> h,
    .extras_var = target_super_extras_var,
    .extras_const = target_super_extras_const
  };
  /* if ((source_super_problem == target_super_problem) && */
  /*     (source_super_solution -> steps_amount == */
  /*      target_super_solution -> steps_amount)) */
  super_problem = (nlmpc_problem_t*) source_super_problem;
  super_problem -> solution_copy (& source_super_solution,
				  & target_super_solution);
  target_solution -> cost = source_solution -> cost;
}

/* int */
/* submc_solution_eval (const nlmpc_optmc_solution_t *solution) */
/* { */
/*   const nlmpc_optmc_problem_t *problem = solution -> problem; */
/*   const submc_parameters_t *parameters = problem -> parameters; */
/*   const nlmpc_problem_t *super_problem = parameters -> super_problem; */
/*   const submc_extras_var_t *extras_var = solution -> extras_var; */
/*   const submc_extras_const_t *extras_const = solution -> extras_const; */
/*   const nlmpc_solution_t super_solution = { */
/*     .problem = (nlmpc_problem_t*) super_problem, */
/*     .steps_amount = problem -> steps_amount, */
/*     .steps_init = problem -> steps_init, */
/*     .steps_final = problem -> steps_final, */
/*     .states_amount = problem -> states_amount, */
/*     .inputs_amount = problem -> inputs_amount, */
/*     .g_amount = problem -> g_amount, */
/*     .h_amount = problem -> h_amount, */
/*     .states = solution -> states, */
/*     .inputs = solution -> inputs, */
/*     .cost = solution -> cost, */
/*     .g = solution -> g, */
/*     .h = solution -> h, */
/*     .extras_var = extras_var -> super_extras_var, */
/*     .extras_const = extras_const -> super_extras_const */
/*   }; */
/*   if (super_problem -> solution_eval) */
/*     return super_problem -> solution_eval (& super_solution); */
/*   else */
/*     return 0; */
/* } */

int
submc_timeout (const nlmpc_optmc_problem_t *problem)
{
  const submc_parameters_t *parameters = problem -> parameters;
  const nlmpc_problem_t *super_problem = parameters -> super_problem;
  return super_problem -> timeout (super_problem);
}
