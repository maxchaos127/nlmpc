
#include "nlmpc/main_fixedtime.h"
#include "nlmpc/main_internals.h"
#include <stdlib.h>

/*** solution:
 ***    All of its members must be allocated.
 ***    Its members "steps_amount", "steps_equiv" and "inputs"
 ***    must be set to their initial estimations.
 ***    The rest of its members may hold arbitrary values.
 ***/
int
nlmpc_main_fixedtime (const nlmpc_problem_t *problem,
		      nlmpc_solution_t *solution,
		      nlmpc_main_fixedtime_status_t *status,
		      nlmpc_main_fixedtime_resources_t *resources,
		      const nlmpc_main_fixedtime_options_t *options)
{
  int fs;
  int flag_alloc;
  nlmpc_main_fixedtime_resources_t _resources_internal;
  nlmpc_main_fixedtime_resources_t *_resources =
    resources ? resources : & _resources_internal;
  nlmpc_optmc_optimizer_t *sub_optimizer =
    & (_resources -> sub_optimizer);
  nlmpc_optmc_optimizer_status_t *sub_optimizer_status =
    & (_resources -> sub_optimizer_status);
  nlmpc_optmc_problem_t *sub_problem =
    & (_resources -> sub_problem);
  nlmpc_optmc_solution_t sub_solution;
  submc_parameters_t *sub_problem_parameters;
  submc_extras_var_t *sub_solution_extras_var;
  submc_extras_const_t *sub_solution_extras_const;

  /*** Initialize status to an invalid state. ***/
  status -> flag_exit_status = -1;
  status -> flag_exit_code = -1;

  /*** Initialize solution. ***/
  problem -> solution_init (solution);
  /*** Make solution consistent. ***/
  /* problem -> simulator (solution -> steps_amount, */
  /* 			0, solution -> steps_amount - 1, */
  /* 			solution -> inputs, solution -> extras_simconst, */
  /* 			solution -> states, solution -> extras_simvar); */
  /* problem -> costfunc (solution -> steps_amount, */
  /* 		       solution -> states, solution -> inputs, */
  /* 		       solution -> extras_simvar, solution -> extras_simconst, */
  /* 		       & (solution -> cost)); */
  /* if (solution -> g_amount) { */
  /*   problem -> gfunc (solution -> steps_amount, */
  /* 		      solution -> states, solution -> inputs, */
  /* 		      solution -> extras_simvar, solution -> extras_simconst, */
  /* 		      solution -> g); */
  /* } */
  /* if (solution -> h_amount) { */
  /*   problem -> hfunc (solution -> steps_amount, */
  /* 		      solution -> states, solution -> inputs, */
  /* 		      solution -> extras_simvar, solution -> extras_simconst, */
  /* 		      solution -> h); */
  /* } */

  flag_alloc = 0;
  if (! flag_alloc)
    {
      sub_problem_parameters =
	(submc_parameters_t*) malloc (sizeof (submc_parameters_t));
      if (! sub_problem_parameters)
	flag_alloc = 1;
    }
  if (! flag_alloc)
    {
      sub_solution_extras_var =
	(submc_extras_var_t*) malloc (sizeof (submc_extras_var_t));
      if (! sub_solution_extras_var)
	flag_alloc = 2;
    }
  if (! flag_alloc)
    {
      sub_solution_extras_const =
	(submc_extras_const_t*) malloc (sizeof (submc_extras_const_t));
      if (! sub_solution_extras_const)
	flag_alloc = 3;
    }
  if (flag_alloc)
    {
      free (sub_solution_extras_const);
      free (sub_solution_extras_var);
      free (sub_problem_parameters);
      status -> flag_exit_status = 0;
      status -> flag_exit_code = 1;
      return 1;
    }

  /*** Construct multi-dimensional constrainted optimization problem. ***/
  sub_problem -> states_dim = problem -> states_dim;
  sub_problem -> inputs_dim = problem -> inputs_dim;
  sub_problem -> steps_amount = solution -> steps_amount;
  sub_problem -> steps_init = solution -> steps_init;
  sub_problem -> steps_final = solution -> steps_final;
  sub_problem -> states_amount = solution -> states_amount;
  sub_problem -> inputs_amount = solution -> inputs_amount;
  sub_problem -> g_amount = solution -> g_amount;
  sub_problem -> h_amount = solution -> h_amount;
  sub_problem -> costfunc = submc_costfunc;
  sub_problem -> gfunc = submc_gfunc;
  sub_problem -> hfunc = submc_hfunc;
  sub_problem -> simulator = submc_simulator;
  sub_problem -> solution_cstr = submc_solution_cstr;
  sub_problem -> solution_dstr = submc_solution_dstr;
  /* sub_problem -> solution_init = submc_solution_init; */
  sub_problem -> solution_copy = submc_solution_copy;
  sub_problem -> timeout = submc_timeout;
  sub_problem_parameters -> super_problem = (nlmpc_problem_t*) problem;
  sub_problem -> parameters = sub_problem_parameters;

  /*** Construct the specified sub_optimizer. ***/
  sub_optimizer -> solver = options -> sub_optimizer_solver;
  sub_optimizer -> options = options -> sub_optimizer_options;
  sub_optimizer -> optimizer_alloc = options -> sub_optimizer_alloc;
  sub_optimizer -> optimizer_free = options -> sub_optimizer_free;
  fs = sub_optimizer -> optimizer_alloc (sub_optimizer, sub_problem);
  if (fs) {
    status -> flag_exit_status = 0;
    status -> flag_exit_code = 1;
    return status -> flag_exit_status;
  }
  
  /*** Construct multi-dimensional constrainted optimization problem. ***/
  sub_solution.problem = sub_problem;
  sub_solution.states = solution -> states;
  sub_solution.inputs = solution -> inputs;
  sub_solution.cost = solution -> cost;
  sub_solution.g = solution -> g;
  sub_solution.h = solution -> h;
  sub_solution_extras_var -> super_extras_var = solution -> extras_var;
  sub_solution.extras_var = sub_solution_extras_var;
  sub_solution_extras_const -> super_extras_const = solution -> extras_const;
  sub_solution.extras_const = sub_solution_extras_const;

  /*** Call the specified sub_optimizer to solve the problem. ***/
  sub_optimizer -> solver (sub_optimizer,
			   & sub_solution,
			   sub_optimizer_status);
  
  /*** Return optimal solution. ***/
  solution -> states = sub_solution.states;
  solution -> inputs = sub_solution.inputs;
  solution -> cost = sub_solution.cost;
  solution -> g = sub_solution.g;
  solution -> h = sub_solution.h;
  sub_solution_extras_var =
    (submc_extras_var_t*) sub_solution.extras_var;
  solution -> extras_var = sub_solution_extras_var -> super_extras_var;
  sub_solution_extras_const =
    (submc_extras_const_t*) sub_solution.extras_const;
  solution -> extras_const = sub_solution_extras_const -> super_extras_const;
  status -> flag_exit_status = sub_optimizer_status -> flag_exit_status;

  /**** Set status. ****/
  status -> flag_exit_status = sub_optimizer_status -> flag_exit_status;
  status -> flag_exit_code = 0;

  /**** Free resources. ****/
  if (resources) {
    sub_optimizer -> optimizer_free (sub_optimizer);
  }
  if (sub_solution_extras_const)
    free (sub_solution_extras_const);
  if (sub_solution_extras_var)
    free (sub_solution_extras_var);
  if (sub_problem_parameters)
    free (sub_problem_parameters);

  return 0;
}
