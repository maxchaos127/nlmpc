
#include "nlmpc/main_freetimemt.h"
#include "nlmpc/main_fixedtime.h"
#include "nlmpc/main_internals.h"
#include <pthread.h>

typedef struct optimthread_pt_wrapper_args_t optimthread_pt_wrapper_args_t;
typedef struct optimthread_t optimthread_t;
static void*
optimthread_pt_wrapper (void *args);

struct optimthread_pt_wrapper_args_t {
  nlmpc_problem_t *problem;
  nlmpc_solution_t *solution;
  nlmpc_main_fixedtime_status_t *status;
  nlmpc_main_fixedtime_resources_t *resources;
  nlmpc_main_fixedtime_options_t *options;
};

struct optimthread_t {
  pthread_t pt;
  int pt_result;
  optimthread_pt_wrapper_args_t pt_wrapper_args;
  unsigned int steps_amount;
  unsigned int steps_init;
  unsigned int steps_final;
  nlmpc_problem_t problem;
  nlmpc_solution_t solution;
  nlmpc_main_fixedtime_status_t status;
  nlmpc_main_fixedtime_resources_t resources;
  nlmpc_main_fixedtime_options_t options;
};

int
nlmpc_main_freetimemt (const nlmpc_problem_t *problem,
		       nlmpc_solution_t *solution,
		       nlmpc_main_freetimemt_status_t *status,
		       nlmpc_main_freetimemt_resources_t *resources,
		       const nlmpc_main_freetimemt_options_t *options)
{
  int fes;
  int flag_alloc;
  int idx;
  const int optimthreads_radius = (int) (options -> optimizations_radius);
  int optimthreads_amount;
  const int optimthreads_amount_limit = 2 * optimthreads_radius + 1;
  const int steps_amount_lb = (int) (options -> steps_amount_lb);
  const int steps_amount_ub = (int) (options -> steps_amount_ub);
  const int steps_amount_mean_desired = (int) (solution -> steps_amount);
  int steps_amount_min, steps_amount_max;
  optimthread_t optimthreads [optimthreads_amount_limit];

  /* unsigned int optimthreads_amount_left, optimthreads_amount_right; */
  /* const unsigned int optimthreads_amount_left_max = */
  /*   ((solution -> steps_amount) - (options -> steps_amount_lb)) / */
  /*   (options -> steps_delta_m); */
  /* const unsigned int optimthreads_amount_right_max = */
  /*   ((options -> steps_amount_ub) - (solution -> steps_amount)) / */
  /*   (options -> steps_delta_p); */
  /* const unsigned int steps_amount_central = solution -> steps_amount; */

  /* unsigned int steps_amount_fixedtime [optimthreads_amount_limit]; */
  /* nlmpc_solution_t solutions_fixedtime [optimthreads_amount_limit]; */
  /* nlmpc_main_fixedtime_status_t status_fixedtime [optimthreads_amount_limit]; */
  /* nlmpc_main_fixedtime_resources_t resources_fixedtime [optimthreads_amount_limit]; */
  /* nlmpc_main_fixedtime_options_t options_fixedtime [optimthreads_amount_limit]; */
  /* pthread_t threads_fixedtime [optimthreads_amount_limit]; */
  /* optimthread_pt_wrapper_args_t threads_fixedtime_args [optimthreads_amount_limit]; */
  /* int threads_fixedtime_res [optimthreads_amount_limit]; */
  unsigned int idx_opt;
  double cost_opt;
  nlmpc_solution_t _solution;



  /**** Initialize status to an invalid state.
   ****/
  status -> flag_exit_status = -1;
  status -> flag_exit_code = -1;

  /**** Validate the radius of the amount of steps.
   ****/
  if (optimthreads_radius < 0)
    {
      status -> flag_exit_status = 0;
      status -> flag_exit_code = 1;
      return 1;
    }
  /**** Validate the limits applied to the amount of steps.
   ****/
  if ((steps_amount_lb < 0) ||
      (steps_amount_ub < 0) ||
      (steps_amount_ub < steps_amount_lb))
    {
      status -> flag_exit_status = 0;
      status -> flag_exit_code = 2;
      return 2;
    }
  if (steps_amount_mean_desired < 0)
    {
      status -> flag_exit_status = 0;
      status -> flag_exit_code = 3;
      return 3;
    }

  /**** Calculate the range of amount of steps of the optimizations.
   ****/
  if ((steps_amount_lb <=
       steps_amount_mean_desired - optimthreads_radius) &&
      (steps_amount_ub >=
       steps_amount_mean_desired + optimthreads_radius))
    {
      steps_amount_min = steps_amount_mean_desired - optimthreads_radius;
      steps_amount_max = steps_amount_mean_desired + optimthreads_radius;
    }
  else if (steps_amount_lb >=
	   steps_amount_mean_desired + optimthreads_radius)
    {
      steps_amount_min = steps_amount_lb;
      if (steps_amount_lb + (optimthreads_amount_limit - 1) <= steps_amount_ub)
	steps_amount_max = steps_amount_lb + (optimthreads_amount_limit - 1);
      else
	steps_amount_max = steps_amount_ub;
    }
  else if (steps_amount_ub <=
	   steps_amount_mean_desired - optimthreads_radius)
    {
      steps_amount_max = steps_amount_ub;
      if (steps_amount_ub - (optimthreads_amount_limit - 1) >= steps_amount_lb)
	steps_amount_min = steps_amount_ub - (optimthreads_amount_limit - 1);
      else
	steps_amount_min = steps_amount_lb;
    }
  else if ((steps_amount_lb >=
	    steps_amount_mean_desired - optimthreads_radius) &&
	   (steps_amount_ub <=
	    steps_amount_mean_desired + optimthreads_radius))
    {
      steps_amount_min = steps_amount_lb;
      steps_amount_max = steps_amount_ub;
    }
  else if (steps_amount_lb >
	   steps_amount_mean_desired - optimthreads_radius)
    {
      steps_amount_min = steps_amount_lb;
      if (steps_amount_ub >= steps_amount_lb + (optimthreads_amount_limit - 1))
	steps_amount_max = steps_amount_lb + (optimthreads_amount_limit - 1);
      else
	steps_amount_max = steps_amount_ub;
    }
  else if (steps_amount_ub <
	   steps_amount_mean_desired + optimthreads_radius)
    {
      steps_amount_max = steps_amount_ub;
      if (steps_amount_lb <= steps_amount_ub - (optimthreads_amount_limit - 1))
	steps_amount_min = steps_amount_ub - (optimthreads_amount_limit - 1);
      else
	steps_amount_min = steps_amount_lb;
    }
  optimthreads_amount = steps_amount_max - steps_amount_min + 1;

  /**** Assign amounts of steps to each optimization's thread.
   ****/
  for (idx = 0; idx < optimthreads_amount; ++ idx)
    {
      optimthreads [idx].steps_amount = steps_amount_min + idx;
      if (optimthreads [idx].steps_amount >= options -> steps_init_offset)
	optimthreads [idx].steps_init = options -> steps_init_offset;
      else
	optimthreads [idx].steps_init = optimthreads [idx].steps_amount;
      if (optimthreads [idx].steps_amount >=
	  optimthreads [idx].steps_init + options -> steps_final_offset)
	optimthreads [idx].steps_final =
	  optimthreads [idx].steps_amount - options -> steps_final_offset;
      else
	optimthreads [idx].steps_final = optimthreads [idx].steps_init;
    }

  /**** Assign fixed-time problems to each optimization's thread.
   ****/
  for (idx = 0; idx < optimthreads_amount; ++ idx)
    {
      optimthreads [idx].problem = *problem;
    }

  /**** Allocate fixed-time solutions to each optimization's thread.
   ****/
  flag_alloc = 0;
  for (idx = 0; idx < optimthreads_amount; ++ idx)
    {
      if (! flag_alloc) {
	fes = problem -> solution_cstr
	  (problem,
	   & (optimthreads [idx].solution),
	   optimthreads [idx].steps_amount,
	   optimthreads [idx].steps_init,
	   optimthreads [idx].steps_final);
	if (fes)
	  {
	    flag_alloc = idx + 1;
	    break;
	  }
      }
    }
  if (flag_alloc)
    {
      for (idx = 0; idx < optimthreads_amount; ++ idx)
	{
	  problem -> solution_dstr (& (optimthreads [idx].solution));
	}
      status -> flag_exit_status = 0;
      status -> flag_exit_code = 4;
      return status -> flag_exit_status;
    }

  /**** Initialize fixed-time options of each optimization's thread.
   ****/
  for (idx = 0; idx < optimthreads_amount; ++ idx)
    {
      optimthreads [idx].options.sub_optimizer_solver =
	options -> sub_optimizer_solver;
    }
  for (idx = 0; idx < optimthreads_amount; ++ idx)
    {
      optimthreads [idx].options.sub_optimizer_options =
	options -> sub_optimizer_options;
    }
  for (idx = 0; idx < optimthreads_amount; ++ idx)
    {
      optimthreads [idx].options.sub_optimizer_alloc =
	options -> sub_optimizer_alloc;
    }
  for (idx = 0; idx < optimthreads_amount; ++ idx)
    {
      optimthreads [idx].options.sub_optimizer_free =
	options -> sub_optimizer_free;
    }

  /**** Construct wrapper's arguments for each optimization's thread.
   ****/
  for (idx = 0; idx < optimthreads_amount; ++ idx)
    {
      optimthreads [idx].pt_wrapper_args.problem =
	& (optimthreads [idx].problem);
    }
  for (idx = 0; idx < optimthreads_amount; ++ idx)
    {
      optimthreads [idx].pt_wrapper_args.solution =
	& (optimthreads [idx].solution);
    }
  for (idx = 0; idx < optimthreads_amount; ++ idx)
    {
      optimthreads [idx].pt_wrapper_args.status =
	& (optimthreads [idx].status);
    }
  for (idx = 0; idx < optimthreads_amount; ++ idx)
    {
      optimthreads [idx].pt_wrapper_args.resources =
	(resources ? & (optimthreads [idx].resources) : NULL);
    }
  for (idx = 0; idx < optimthreads_amount; ++ idx)
    {
      optimthreads [idx].pt_wrapper_args.options =
	& (optimthreads [idx].options);
    }

  /**** Solve fixed-time problems.
   ****/
  for (idx = 0; idx < optimthreads_amount; ++ idx)
    {
      optimthreads [idx].pt_result =
	pthread_create (& (optimthreads [idx].pt),
			NULL,
			optimthread_pt_wrapper,
			(void*) & (optimthreads [idx].pt_wrapper_args));
    }
  for (idx = 0; idx < optimthreads_amount; ++ idx)
    {
      pthread_join (optimthreads [idx].pt, NULL);
    }

  /**** Return optimal solution.
   ****/
  idx_opt = 0;
  cost_opt = optimthreads [0].solution.cost;
  for (idx = 1; idx < optimthreads_amount; ++ idx)
    {
      if (optimthreads [idx].solution.cost < cost_opt)
	{
	  idx_opt = idx;
	  cost_opt = optimthreads [idx].solution.cost;
	}
    }
  _solution = *solution;
  *solution = optimthreads [idx_opt].solution;
  optimthreads [idx_opt].solution = _solution;

  /**** Free resources.
   ****/
  for (idx = 0; idx < optimthreads_amount; ++ idx)
    {
      problem -> solution_dstr (& (optimthreads [idx].solution));
    }

  return status -> flag_exit_status;
}



static void*
optimthread_pt_wrapper (void *args)
{
  optimthread_pt_wrapper_args_t *_args = args;
  nlmpc_main_fixedtime (_args -> problem, _args -> solution,
			_args -> status, _args -> resources, _args -> options);
  return NULL;
}
