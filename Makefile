
SHELL = /bin/bash
.SHELLFLAGS = -ec

.DELETE_ON_ERROR:

SOURCE_DIR := src
INCLUDE_DIR := include

BUILD_DIR := build
AUTODEP_DIR := $(addprefix $(BUILD_DIR)/,autodeps)
OBJECT_DIR := $(addprefix $(BUILD_DIR)/,objects)
BIN_DIR := $(addprefix $(BIN_DIR)/,bin)
LIB_DIR := $(addprefix $(BUILD_DIR)/,lib)

EXTERNAL_INCLUDE_DIRS :=

vpath %.c $(SOURCE_DIR)
vpath %.h $(SOURCE_DIR) $(INCLUDE_DIR) $(EXTERNAL_INCLUDE_DIRS)
vpath %.o $(OBJECT_DIR)
vpath %.d $(AUTODEP_DIR)
vpath %.a $(LIB_DIR)

sources := $(shell find $(SOURCE_DIR) -type f -iname '*.c')
headers := $(shell find $(SOURCE_DIR) $(INCLUDE_DIR) -type f -iname '*.h')
objects := $(patsubst $(SOURCE_DIR)/%.c,$(OBJECT_DIR)/%.o,$(sources))
autodeps := $(patsubst $(SOURCE_DIR)/%.c,$(AUTODEP_DIR)/%.d,$(sources))

objects_optmc_alm := \
	$(shell echo $(objects) | grep -Eo '\S+_alm[^a-zA-Z0-9]\S+')
objects_optmunc_lbfgs := \
	$(shell echo $(objects) | grep -Eo '\S+_lbfgs[^a-zA-Z0-9]\S+')
objects_optsunc_interpquad := \
	$(shell echo $(objects) | grep -Eo '\S+_interpquad[^a-zA-Z0-9]\S+')

SOURCE_SUBDIRS := $(sort $(dir $(sources)))
INCLUDE_SUBDIRS := $(sort $(dir $(headers)))
OBJECT_SUBDIRS := $(sort $(dir $(objects)))
AUTODEP_SUBDIRS := $(sort $(dir $(autodeps)))

CC = gcc
CFLAGS += -Wall -g
CINCLUDE += -I$(INCLUDE_DIR) $(addprefix -I,$(EXTERNAL_INCLUDE_DIRS))

.PHONY: all_objects
all_objects: $(objects)

libnlmpc.a: $(objects) | $(LIB_DIR)
	ar rcs $(addprefix $(LIB_DIR)/,$@) $(objects)

.PHONY: optmc_alm
optmc_alm: $(objects_optmc_alm)
.PHONY: optmunc_lbfgs
optmunc_lbfgs: $(objects_optmunc_lbfgs)
.PHONY: optsunc_interpquad
optsunc_interpquad: $(objects_optsunc_interpquad)

$(TEST_TARGETS): libnlmpc.a



-include $(autodeps)

$(BUILD_DIR):
	-mkdir -p $@ ;

$(LIB_DIR):
	-mkdir -p $@ ;

$(OBJECT_DIR)/%.o %.o: %.c | $(OBJECT_SUBDIRS)
	$(CC) $(CFLAGS) -c $< \
	$(CINCLUDE) \
	-o $(OBJECT_DIR)/$*.o

#	$(addprefix -I,$(sort $(dir $(filter %.h,$^)))) \

$(OBJECT_SUBDIRS):
	-mkdir -p $@ ;

$(AUTODEP_DIR)/%.d %.d: %.c | $(AUTODEP_SUBDIRS)
	@set -e ; ad="$(AUTODEP_DIR)/$*.d" ; rm -f $${ad} ; \
	$(CC) -MM $(CFLAGS) $(CINCLUDE) $< > $${ad}.$$$$ ; \
	sed 's,\($(notdir $*)\)\.o[ :]*,$*.o '"$${ad}"' : ,g' \
	< $${ad}.$$$$ > $${ad} ; \
	rm -f $${ad}.$$$$ ;
$(AUTODEP_SUBDIRS):
	-mkdir -p $@ ;

.PHONY: echo_status
echo_status:
	@echo "SOURCES:" $(sources); \
	echo "OBJECTS:" $(objects); \
	echo "OBJECT_SUBDIR:" $(OBJECT_SUBDIRS); \
	echo "AUTODEPS:" $(autodeps); \
	echo "AUTODEPS_SUBDIRS:" $(AUTODEP_SUBDIRS); \
	echo "INCLUDE_DIR:" $(CINCLUDE)

.PHONY: clean clean_d clean_objects clean_libs
clean: clean_d clean_objects clean_libs
clean_d:
	-find $(AUTODEP_SUBDIRS) -type f -iname '*.d' | xargs rm -f ;
clean_objects:
	-find $(OBJECT_SUBDIRS) -type f -iname '*.o' | xargs rm -f ;
clean_libs:
	-find $(LIB_DIR) -type f -iname '*.a' | xargs rm -f ;
